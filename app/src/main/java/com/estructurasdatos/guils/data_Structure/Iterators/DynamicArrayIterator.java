package com.estructurasdatos.guils.data_Structure.Iterators;

import com.estructurasdatos.guils.data_Structure.DynamicArray;

import java.util.Iterator;

public class DynamicArrayIterator<T> implements Iterator {
    int loop=0;
    int position;
    T[] array;

    public DynamicArrayIterator(DynamicArray<T> array){
        this.position=array.getPosition();
        this.array=array.getArray();
    }

    @Override
    public boolean hasNext() {
        return loop<position;
    }

    @Override
    public Object next() {
        T ob = array[loop];
        loop++;
        return ob;

    }

}
