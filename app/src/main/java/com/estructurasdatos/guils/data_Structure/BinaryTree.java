package com.estructurasdatos.guils.data_Structure;

import android.util.Log;

import com.estructurasdatos.guils.data_Structure.interfaces.BinaryTreeInterface;

public class BinaryTree<T> implements BinaryTreeInterface{
    private NodeTree<T> root;
    private int size;

    public NodeTree<T> getRoot() {
        return root;
    }

    public BinaryTree() {
        this.root = null;
        this.size = 0;
    }

    private NodeTree<T> rightSift(NodeTree<T> subTree){
        NodeTree<T> auxLeft = subTree.getLeft();
        if (auxLeft.getRight()!=null) {
            subTree.setLeft(auxLeft.getRight());
            auxLeft.getRight().setParent(subTree);
        } else {
            subTree.setLeft(null);
        }
        auxLeft.setRight(subTree);
        subTree.setParent(auxLeft);
        subTree.setBalanceFactor(Math.max(subTree.calBalanceFactor(subTree.getRight()),subTree.calBalanceFactor(subTree.getLeft()))+1);
        auxLeft.setBalanceFactor(Math.max(subTree.calBalanceFactor(auxLeft.getRight()),subTree.calBalanceFactor(auxLeft.getLeft()))+1);
        return auxLeft;
    }

    private NodeTree<T> leftSift(NodeTree<T> subTree){
        NodeTree<T> auxRight = subTree.getRight();
        if (auxRight.getLeft()!=null) {
            subTree.setRight(auxRight.getLeft());
            auxRight.getLeft().setParent(subTree);
        }else{
            subTree.setRight(null);
        }
        auxRight.setLeft(subTree);
        subTree.setParent(auxRight);
        subTree.setBalanceFactor(Math.max(subTree.calBalanceFactor(subTree.getRight()),subTree.calBalanceFactor(subTree.getLeft()))+1);
        auxRight.setBalanceFactor(Math.max(subTree.calBalanceFactor(auxRight.getRight()),subTree.calBalanceFactor(auxRight.getLeft()))+1);
        return auxRight;

    }

    private NodeTree<T> rightDoubleSift(NodeTree<T> subTree){
        NodeTree<T> aux=leftSift(subTree.getLeft());
        subTree.setLeft(aux);
        aux.setParent(subTree);
        return rightSift(subTree);
    }

    private NodeTree<T> leftDoubleSift(NodeTree<T> subTree){
        NodeTree<T> aux=rightSift(subTree.getRight());
        subTree.setRight(aux);
        aux.setParent(subTree);
        return leftSift(subTree);
    }

    private NodeTree<T> insert(NodeTree<T> newNode, NodeTree<T>  subTree){
        if (subTree==null){
            subTree=newNode;
        }else if (newNode.compare(subTree)>0){
            if (subTree.getRight()==null){
                subTree.setRight(newNode);
                newNode.setParent(subTree);
            }else {
                NodeTree<T> aux=insert(newNode,subTree.getRight());
                subTree.setRight(aux);
                aux.setParent(subTree);
                if (subTree.calBalanceFactor(subTree.getRight())-subTree.calBalanceFactor(subTree.getLeft())==2){
                    if(newNode.compare(subTree.getRight())>0){
                        NodeTree<T> aux1=leftSift(subTree);
                        subTree=aux1;
                        aux1.setParent(subTree);
                    }else {
                        NodeTree<T> aux2=leftDoubleSift(subTree);
                        subTree=aux2;
                        aux2.setParent(subTree);
                    }
                }
            }
        }else if (newNode.compare(subTree)<0){
            if (subTree.getLeft()==null){
                subTree.setLeft(newNode);
                newNode.setParent(subTree);
            }else {
                NodeTree<T> aux=insert(newNode,subTree.getLeft());
                subTree.setLeft(aux);
                aux.setParent(subTree);
                if (subTree.calBalanceFactor(subTree.getRight())-subTree.calBalanceFactor(subTree.getLeft())==-2){
                    if (newNode.compare(subTree.getRight())<0){
                        NodeTree<T> aux1=rightSift(subTree);
                        subTree=aux1;
                        aux1.setParent(subTree);

                    }else {
                        NodeTree<T> aux2=rightDoubleSift(subTree);
                        subTree=aux2;
                        aux2.setParent(subTree);
                    }
                }
            }
        }else {
            //Log.e("","Nodo duplicado");
            System.out.println("Nodo duplicado");
        }
        if (subTree.getRight()!=null&&subTree.getLeft()==null){
            subTree.setBalanceFactor(subTree.getRight().getBalanceFactor()+1);
        }else if (subTree.getRight()==null&&subTree.getLeft()!=null){
            subTree.setBalanceFactor(subTree.getLeft().getBalanceFactor()+1);
        }else if(subTree.getRight()==null&&subTree.getLeft()==null){

        }else {
            subTree.setBalanceFactor(Math.max(subTree.calBalanceFactor(subTree.getRight()),subTree.calBalanceFactor(subTree.getLeft()))+1);
        }
        return subTree;
    }


    @Override
    public void insert(Object key) {
        NodeTree<T> newNode = new NodeTree<>((T)key);
        this.root=insert(newNode, this.root);

    }

    private NodeTree<T> erase(boolean left, NodeTree<T> subTree){
        if(left){
            if(subTree.getLeft().getLeft()==null&&subTree.getLeft().getRight()==null){
                subTree.setLeft(null);
            }else if (subTree.getLeft().getLeft()!=null&&subTree.getLeft().getRight()==null){
                subTree.setLeft(subTree.getLeft().getLeft());
                subTree.getLeft().getLeft().setParent(subTree);
            }else if(subTree.getLeft().getLeft()==null&&subTree.getLeft().getRight()!=null){
                subTree.setLeft(subTree.getLeft().getRight());
                subTree.getLeft().getRight().setParent(subTree);
            }else{
                NodeTree<T> lefty = subTree.getLeft();
                while(lefty.getLeft()!=null&&lefty.getLeft().getLeft()!=null){
                    if(lefty.getRight()==null){
                        lefty.setBalanceFactor(lefty.getBalanceFactor()-1);
                    }
                    lefty=lefty.getLeft();
                }
                if(lefty.getRight()==null){
                    lefty.setBalanceFactor(lefty.getBalanceFactor()-1);
                }
                NodeTree<T> aux = lefty.getLeft();
                if(aux.getLeft()!=null){
                    lefty.setLeft(aux.getLeft());
                    aux.getLeft().setParent(lefty);
                }else{
                    lefty.setLeft(null);
                }
                aux.setLeft(subTree.getLeft().getLeft());
                subTree.getLeft().getLeft().setParent(aux);
                aux.setRight(subTree.getLeft().getRight());
                subTree.getLeft().getRight().setParent(aux);
                subTree.setLeft(aux);
                aux.setParent(subTree);
                subTree.setBalanceFactor(Math.max(subTree.getLeft().getBalanceFactor(), subTree.getRight().getBalanceFactor())+1);
            }
            if (subTree.calBalanceFactor(subTree.getRight())-subTree.calBalanceFactor(subTree.getLeft())==2){
                subTree=leftSift(subTree);
            }
        }else{
            if(subTree.getRight().getLeft()==null&&subTree.getRight().getRight()==null){
                subTree.setRight(null);
            }else if (subTree.getRight().getLeft()!=null&&subTree.getRight().getRight()==null){
                subTree.setRight(subTree.getRight().getLeft());
                subTree.getRight().getLeft().setParent(subTree);
            }else if(subTree.getRight().getLeft()==null&&subTree.getRight().getRight()!=null){
                subTree.setRight(subTree.getRight().getRight());
                subTree.getRight().getRight().setParent(subTree);
            }else{
                NodeTree<T> lefty = subTree.getRight();
                while(lefty.getLeft()!=null&&lefty.getLeft().getLeft()!=null){
                    if(lefty.getRight()==null){
                        lefty.setBalanceFactor(lefty.getBalanceFactor()-1);
                    }
                    lefty=lefty.getLeft();
                }
                if(lefty.getRight()==null){
                    lefty.setBalanceFactor(lefty.getBalanceFactor()-1);
                }
                NodeTree<T> aux = lefty.getLeft();
                if(aux.getLeft()!=null){
                    lefty.setLeft(aux.getLeft());
                    aux.getLeft().setParent(lefty);
                }else{
                    lefty.setLeft(null);
                }
                aux.setLeft(subTree.getRight().getLeft());
                subTree.getRight().getLeft().setParent(aux);
                aux.setRight(subTree.getRight().getRight());
                subTree.getRight().getRight().setParent(aux);
                subTree.setRight(aux);
                aux.setParent(subTree);
                subTree.setBalanceFactor(Math.max(subTree.getLeft().getBalanceFactor(), subTree.getRight().getBalanceFactor())+1);
            }
            if (subTree.calBalanceFactor(subTree.getRight())-subTree.calBalanceFactor(subTree.getLeft())==-2){
                subTree=rightSift(subTree);
            }
        }
        return subTree;
    }

    private NodeTree<T> delete(NodeTree<T> newNode, NodeTree<T> subTree){
        NodeTree<T> aux = subTree;
        switch (newNode.compare(aux)) {
            case 1:
                if(newNode.compare(aux.getRight())==0){
                    aux=erase(false, aux);
                }else{
                    NodeTree<T> aux1=delete(newNode, aux.getRight());
                    aux.setRight(aux1);
                    aux1.setParent(aux);
                }
                break;
            case -1:
                if(newNode.compare(aux.getLeft())==0){
                    aux=erase(true, aux);
                }else{
                    NodeTree<T> aux1=delete(newNode, aux.getLeft());
                    aux.setLeft(aux1);
                    aux1.setParent(aux);
                }
                break;
            default:
                if(aux.getLeft()==null&&aux.getRight()==null){
                    aux=null;
                }else if (aux.getLeft()!=null&&aux.getRight()==null){
                    aux=aux.getLeft();
                }else if(aux.getLeft()==null&&aux.getRight()!=null){
                    aux=aux.getRight();
                }else{
                    NodeTree<T> lefty = aux.getRight();
                    if(lefty.getLeft()!=null){
                        while(lefty.getLeft()!=null&&lefty.getLeft().getLeft()!=null){
                            if(lefty.getRight()==null){
                                lefty.setBalanceFactor(lefty.getBalanceFactor()-1);
                            }
                            lefty=lefty.getLeft();
                        }
                        if(lefty.getRight()==null){
                            lefty.setBalanceFactor(lefty.getBalanceFactor()-1);
                        }
                        NodeTree<T> newAux = lefty.getLeft();
                        if(newAux.getLeft()!=null){
                            lefty.setLeft(newAux.getLeft());
                            newAux.getLeft().setParent(lefty);
                        }else{
                            lefty.setLeft(null);
                        }
                        newAux.setLeft(aux.getLeft());
                        aux.getLeft().setParent(newAux);
                        newAux.setRight(aux.getRight());
                        aux.getRight().setParent(newAux);
                        this.root=newAux;

                    }else{
                        lefty.setLeft(aux.getLeft());
                        aux.getLeft().setParent(lefty);
                        this.root=lefty;
                    }
                    this.root.setBalanceFactor(Math.max(this.root.getLeft().getBalanceFactor(), this.root.getRight().getBalanceFactor())+1);
                    if (this.root.calBalanceFactor(this.root.getRight())-this.root.calBalanceFactor(this.root.getLeft())==2){
                        this.root=leftSift(this.root);
                    }else{
                        this.root=rightSift(this.root);
                    }
                    return this.root;
                }
        }
        return aux;
    }

    @Override
    public void delete(Object key) {
        NodeTree<T> newNode = new NodeTree<>((T)key);
        this.root=delete(newNode, this.root);
    }

    public  void levelOrder(NodeTree<T> root) {
        Queue<NodeTree<T>> queue = new Queue();
        queue.enqueue(root);
        while(!queue.isEmpty()){
            NodeTree<T> aux = queue.dequeue();
            //System.out.print(aux.getElement()+" ");
            if(aux.getLeft()!=null)queue.enqueue(aux.getLeft());
            if(aux.getRight()!=null)queue.enqueue(aux.getRight());
        }

    }

    public DynamicArray<T> inOrder(NodeTree<T> root) {
        DynamicArray<T> array =new DynamicArray<>();
        Stack<NodeTree<T>> stack = new Stack();
        stack.push(root);
        while(!stack.isEmpty()){
            if(root.getLeft()!=null){
                root=root.getLeft();
                stack.push(root);
            }else{
                root=stack.pop();
                array.add(array.getPosition(), root.getElement());
                if(root.getRight()!=null){
                    root=root.getRight();
                    stack.push(root);
                }
            }
        }
        return array;

    }

    private NodeTree<T> findLeft(NodeTree<T> subTree){
        while(subTree.getLeft().getElement()!=null){
            subTree=subTree.getLeft();
        }
        return subTree;
    }

    @Override
    public NodeTree<T> find(Object key) {
        NodeTree<T> nodeTree = new NodeTree<>((T)key);
        NodeTree<T> aux = this.root;
        while(aux.compare(nodeTree)!=0&&(aux.getLeft()!=null||aux.getRight()!=null)){
            if(aux.compare(nodeTree)==-1&&aux.getRight()!=null){
                aux=aux.getRight();
            }else if(aux.compare(nodeTree)==1&&aux.getLeft()!=null){
                aux=aux.getLeft();
            }else{
                return null;
            }
        }
        if(aux.compare(nodeTree)==0){
            return aux;
        }else{
            return null;
        }
    }

    private NodeTree<T> findRange(Object key,NodeTree<T> subTree){
        NodeTree<T> nodeTree = new NodeTree<>((T)key);
        switch(subTree.compare(nodeTree)){
            case 0:
                return subTree;
            case 1:
                if(subTree.getLeft()!=null){
                    return findRange(key, subTree.getLeft());
                }else{
                    return subTree;
                }
            case -1:
                if(subTree.getRight()!=null){
                    return findRange(key, subTree.getRight());
                }else{
                    return subTree;
                }
            default:
                return null;
        }
    }

    @Override
    public NodeTree next(NodeTree n) {
        if(n.getRight()!=null){
            return leftDescendant(n.getRight());
        }else{
            return rightAncestor(n);
        }
    }

    private NodeTree<T> leftDescendant(NodeTree n){
        if(n.getLeft()==null){
            return n;
        }else{
            return leftDescendant(n.getLeft());
        }
    }

    private NodeTree<T> rightAncestor(NodeTree n){
        if(n.compare(n.getParent())==-1){
            return n.getParent();
        }else{
            return rightAncestor(n.getParent());
        }
    }

    @Override
    public DynamicArray<NodeTree<T>> rangeSearch(Object x, Object y) {
        DynamicArray<NodeTree<T>> array = new DynamicArray<>();
        NodeTree<T> nodeTree = findRange(x, this.root);
        NodeTree<T> inf =new NodeTree(x);
        NodeTree<T> sup =new NodeTree(y);
        int position = 0;
        while(nodeTree.compare(sup)!=1){
            if(nodeTree.compare(inf)!=-1){
                array.add(position, nodeTree);
                position++;
            }
            nodeTree=next(nodeTree);
        }
        return array;
    }


}
