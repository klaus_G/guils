/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.estructurasdatos.guils.data_Structure.interfaces;;

/**
 *
 * @author Flexxo333
 */
public interface Entry<K,V> {
    
    //Retorna la llave almacenada en esta entrada
    public K getKey();
    
    //Retorna el valor almacenado en esta entrada
    public V getValue();
}
