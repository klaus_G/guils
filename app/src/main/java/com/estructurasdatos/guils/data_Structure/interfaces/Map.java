/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.estructurasdatos.guils.data_Structure.interfaces;;

import java.security.InvalidKeyException;


public interface Map<K,V> {
    
    //Retorna el numero de items en el mapa
    public int size();
    
    //¿Está vacío el mapa?
    public boolean isEmpty();
    
    //Ubica un par llave-valor en el mapa, 
    public V put(K key, V value) throws InvalidKeyException;
    
    //Retorna el valor asociado con una llave
    public V get(K key) throws InvalidKeyException;
    
    //Retira el par llave-valor dada una llave
    public V remove(K key) throws InvalidKeyException;
    
    //Retorna un objeto iterable que contiene todas las llaves en el mapa
    public Iterable<K> keySet();
    
    //Retorna un objeto iterable que contiene todos los valores en el mapa
    public Iterable<V> values();
    
    //Retorna un objeto iterable que contiene todas las entradas e el mapa
    public Iterable<Entry<K,V>> entrySet();
    
}
