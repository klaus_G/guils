/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.estructurasdatos.guils.data_Structure;;

import com.estructurasdatos.guils.data_Structure.interfaces.Entry;
import com.estructurasdatos.guils.data_Structure.interfaces.Map;

import java.security.InvalidKeyException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HashTableMap<K,V> implements Map<K,V> {

    

    public static class HashEntry<K,V> implements Entry<K,V> {
        protected K key;
        protected V value;

        public HashEntry(K k, V v){
            this.key = k;
            this.value = v;
        }

        public V getValue(){
            return this.value;
        }

        public K getKey(){
            return this.key;
        }

        public V setValue(V val){
            V oldValue = value;
            this.value = val;
            return oldValue;
        }

        public boolean equals(Object o){
            HashEntry entry;
            try{
                entry = (HashEntry) o; //Probar si o es un objeto de tipo HashEntry
            } catch (ClassCastException ex){
                return false;
            }
            return (entry.getKey() == this.key)  && (entry.getValue() == this.value);
        }

        public String toString(){
            return "(" +  this.key + "," + this.value + ")";
        }
    
    }
    
    protected Entry<K,V> AVAILABLE = new HashEntry<K,V>(null, null);
    
    protected int numberOfEntries; //Numero de entradas actualmente en el mapa
    protected int prime; //Numero primo usado en la función hash
    protected int capacity; // Capacidad del arreglo de contenedores
    
    public Entry<K,V>[] bucket; //contenedores
    
    protected long scale;   //factor de escala (a)
    protected long shift;   //factor de escala (b)
    
    public HashTableMap(){
        this(109345121,1000);
    }
    
    public HashTableMap(int cap){
        this(109345121, cap);
    }
    
    public HashTableMap(int p, int cap){
        this.prime = p;
        this.capacity = cap;
        
        bucket = (Entry<K,V>[]) new HashEntry[capacity];    //Crear un arreglo de HashEntries
        
        java.util.Random rand = new java.util.Random();
        
        scale = rand.nextInt(prime-1) + 1;  // entero perteneciente a [1..(p-1)]
        shift = rand.nextInt(prime);        // entero perteneciente a [0..(p-1)]
    }
    
    protected void checkKey(K key) throws InvalidKeyException{
        if ( key == null) {
            throw new InvalidKeyException("Invalid key: null");
        }
    }
    
    public int hashValue(K key){
        return (int)((Math.abs(key.hashCode()*this.scale+this.shift) % this.prime) % this.capacity);
    }
    
    /**
     Método de busqueda:
     * retorna index >= 0 si la llave se encuentra
     * retorna un (-a -1) valor negativo, donde a es el indice del primer espacio
     * vacío/disponible2     
     **/
    protected int findEntry(K key){
        int available = -1;
        int i, j;
        
        try {
            checkKey(key);  //Revisar el tipo de objeto de la llave
        } catch (InvalidKeyException ex) {
            Logger.getLogger(HashTableMap.class.getName()).log(Level.SEVERE, null, ex);
        }
        i = hashValue(key);
        j = i;
        
        do {
            Entry<K,V> e = bucket[i];
            
            if(e == null){
                //Encontrar un espacio vacío
                if (available < 0){
                    available = i;
                }
                break;
            }
            
            if(key.equals(e.getKey())){ //Se encontró la llave
                return i; //Llave encontrada, retornan el indice del arreglo donde se almacena
            }
            
            if (e == AVAILABLE){ //El contenedor está desactivado
                if(available <0){
                    available = i; //recordar el primer espacio vacío
                }
            }
            
            i = (i+1) % capacity; //Ver la siguiente ubicación
        } while ( i!= j);
        
        return -(available +1);
        
    }
    
    
    
    @Override
    public int size() {
        return numberOfEntries;
    }

    @Override
    public boolean isEmpty() {
        return (numberOfEntries == 0);
    }

    @Override
    public V get(K key) throws InvalidKeyException{
        int i;
        
        i = findEntry(key);
        
        if (i < 0){
            return null; //Si no hay valor para la llave...
        }
        
        return bucket[i].getValue();
    }
    
    @Override
    public V put(K key, V value) throws InvalidKeyException {
        int i;
        HashEntry<K,V> oldValue;
        
        i = findEntry(key);
        
        if (i >= 0) {
            //oldValue = (HashEntry<K, V>) ((HashEntry<K,V>) bucket[i]).setValue(value);
            //return (oldValue).getValue();
        }
        
        //¿Es necesario un re-hashing?
        if ( numberOfEntries >= capacity/2 ) { //hacer rehashing para mantener el factor de carga en 0.5 como máximo
            rehash();
            i = findEntry(key); //Encontrar el espacio adecuado
        }
        int k = (-i);
        int m = k-1;
        bucket[m] = new HashEntry<K,V>(key, value);
        numberOfEntries++;
        return null; //No habia un valor previo
    }

    protected void rehash(){
        int i, j, k;
        capacity = 2*capacity;
        Entry<K,V>[] old = bucket; //Copiar el arreglo previo
        bucket = (Entry<K,V>[]) new Entry[capacity]; //El nuevo contenedor tiene el doble del tamaño
        java.util.Random rand = new java.util.Random();
        scale = rand.nextInt(prime-1) + 1;
        shift = rand.nextInt(prime);
        for (i = 0; i < old.length; i++) {
            Entry<K,V> e = old[i];
            if ( (e != null) && ( e!= AVAILABLE ) ) { //una entrada valida
                k = findEntry(e.getKey());
                j = (-k) - 1; //k debe ser negativo ya que la llave es nueva
                bucket[j] = e;
            }
        }
        
    }

    @Override
    public V remove(K key) throws InvalidKeyException {
        int i = findEntry(key);
        if ( i<0 ) {
            return null;
        }
        V removed = bucket[i].getValue();
        bucket[i] = AVAILABLE;
        numberOfEntries--;
        return removed;
    }

    @Override
    public Iterable<K> keySet() {
        LinkedList<K> keys=new LinkedList<>();
        for (int i=0; i < capacity; i++) 
        {
          if ((bucket[i] != null) && (bucket[i] != AVAILABLE)) 
          {
            keys.pushBack(bucket[i].getKey());
          }
        }

        return keys;
    }

    @Override
    public Iterable<V> values() {
        LinkedList<V> values=new LinkedList<V>();
        

        for (int i=0; i<capacity; i++) 
        if ((bucket[i] != null) && (bucket[i] != AVAILABLE)) 
          values.pushBack(bucket[i].getValue());
        return values;
    }

    @Override
    public Iterable<Entry<K, V>> entrySet() {
        LinkedList<Entry<K,V>> entries=new LinkedList<Entry<K,V>>();
        for (int i=0; i<capacity; i++) 
          if ((bucket[i] != null) && (bucket[i] != AVAILABLE)) 
            entries.pushBack(bucket[i]);
        return entries;
    }
    
}
