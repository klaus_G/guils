package com.estructurasdatos.guils.data_Structure.interfaces;

public interface QueueInterface<T> {
    void enqueue(T key);
    T dequeue();
    T peek();
    boolean isEmpty();
    String print();
}
