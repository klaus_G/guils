package com.estructurasdatos.guils.data_Structure;

import com.estructurasdatos.guils.data_Structure.interfaces.QueueInterface;

public class Queue<T> implements QueueInterface<T> {

    private LinkedList<T> list;

    @Override
    public void enqueue(T key) {
        list.pushBack(key);
    }

    @Override
    public T dequeue() {
        return list.popFront();
    }

    @Override
    public T peek() {
        return list.topFront();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public String print() {
        return null;
    }
}
