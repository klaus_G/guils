package com.estructurasdatos.guils.data_Structure;

public class NodeLinkedList<T>{
    T element;
    NodeLinkedList<T> next;

    public NodeLinkedList(){
        this.element=null;
        this.next=null;
    }

    public NodeLinkedList(T element){
        this.element=element;
        this.next=null;
    }

    NodeLinkedList(T element,NodeLinkedList<T> next){
        this.element=element;
        this.next=next;
    }

    public T getElement(){
        return this.element;
    }

    public NodeLinkedList<T> getNext(){
        return this.next;
    }
}
