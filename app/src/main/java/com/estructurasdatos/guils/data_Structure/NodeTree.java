package com.estructurasdatos.guils.data_Structure;

import com.estructurasdatos.guils.model.Trip;

public class NodeTree<T> {
    private NodeTree<T> right;
    private NodeTree<T> left;
    private NodeTree<T> parent;
    private T element;
    private int balanceFactor;

    public NodeTree() {
        this.right=null;
        this.left=null;
        this.parent=null;
        this.element=null;
        this.balanceFactor=0;
    }

    public NodeTree(T element) {
        this.element = element;
        this.parent=null;
        this.right=null;
        this.left=null;

    }

    public NodeTree(NodeTree<T> right, NodeTree<T> prev, T element) {
        this.right = right;
        this.left = prev;
        this.element = element;
    }

    public NodeTree<T> getRight() {
        return right;
    }

    public void setRight(NodeTree<T> right) {
        this.right = right;
    }

    public NodeTree<T> getLeft() {
        return left;
    }

    public void setLeft(NodeTree<T> left) {
        this.left = left;
    }

    public NodeTree<T> getParent() {
        return parent;
    }

    public void setParent(NodeTree<T> parent) {
        this.parent = parent;
    }

    public T getElement() {
        return element;
    }

    public void setElement(T element) {
        this.element = element;
    }

    public int getBalanceFactor() {
        return balanceFactor;
    }

    public void setBalanceFactor(int balanceFactor) {
        this.balanceFactor = balanceFactor;
    }

    public int calBalanceFactor (NodeTree<T> node){
        if (node==null){
            return -1;
        }else {
            return node.balanceFactor;
        }
    }

    public int compare(NodeTree<T> o) {

        if (o.element.getClass().equals(Integer.class)){
            if((Integer)this.element>(Integer)((NodeTree<T>)o).element){
                return 1;
            }else if((Integer)this.element<(Integer)((NodeTree<T>)o).element){
                return -1;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }
}
