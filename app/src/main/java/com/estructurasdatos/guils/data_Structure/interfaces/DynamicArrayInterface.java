package com.estructurasdatos.guils.data_Structure.interfaces;

public interface DynamicArrayInterface<T> {
    T get(int i);
    void set(int i,T val);
    void pushBack(T val);
    void remove(int i);
    int size();
}
