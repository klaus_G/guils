package com.estructurasdatos.guils.data_Structure.interfaces;

import com.estructurasdatos.guils.data_Structure.DynamicArray;

public interface BinaryHeapInterface<T> {
    T getMax();
    void insert(T key);
    void siftUp(int i);
    void siftDown(int i);
    T extractMax();
    T remove(int i);
    void changePriority(int i, int key);
    DynamicArray<T> heapSort(T[] array);
    void partialSorting(T[] array,int k);
    void buildHeap(T[] array);
}
