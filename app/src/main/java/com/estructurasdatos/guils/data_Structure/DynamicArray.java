package com.estructurasdatos.guils.data_Structure;

import com.estructurasdatos.guils.data_Structure.interfaces.DynamicArrayInterface;
import com.estructurasdatos.guils.util.TimeCount;

public class DynamicArray<T> implements DynamicArrayInterface<T> {

    public T[] array;
    private int size;
    private int position;


    public T[] getArray() {
        return array;
    }

    public void setArray(T[] array) {
        this.array = array;
        this.size=array.length;
        this.position=this.size-1;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getPosition() {

        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public DynamicArray() {
        this.size = 1;
        this.position = 0;
        this.array = (T[])new Object[1];
    }

    public void removeAll(){
        array=(T[])new Object[1];
        size=1;
        position=0;
    }

    @Override
    public T get(int i) {
        if (i<=position) {
            return array[i];
        }else{
            return null;
        }
    }

    public void add(int i,T val){
        if(position==size-1){
            size*=2;
            T[] duplicate = (T[]) new Object[size];
            for (int j=0; j<=position; j++) {
                duplicate[j]=array[j];
            }
            this.array=duplicate;
        }
        for(int j=position;j>=i;j--){
            array[j+1]=array[j];
        }

        array[i]=val;
        position++;
    }

    @Override
    public void set(int i, T val) {
        if (i<=position) {
            array[i] = val;
        }else{

        }
    }

    @Override
    public void pushBack(T val) {
        if (position==size-1){
            size*=2;
            T[] duplicate = (T[]) new Object[size];
            for (int i=0; i<=position; i++) {
                duplicate[i]=array[i];
            }

            duplicate[position]=val;
            position++;
            this.array=duplicate;
        }else{
            array[position]=val;
            position++;
        }
    }

    @Override
    public void remove(int i) {
        if (position==0||i>position){

        }else{
            for(int j=i;j<position;j++){
                array[j]=array[j+1];
            }
            array[position]=null;
            position--;
        }
    }

    @Override
    public int size() {
        return position;
    }

    public void sort() {
        BinaryHeap<T> binaryHeap=new BinaryHeap<>();
        TimeCount timeCount=new TimeCount();

        this.array=binaryHeap.heapSort(this.array).getArray();
        

    }
}
