package com.estructurasdatos.guils.data_Structure.interfaces;

import com.estructurasdatos.guils.data_Structure.NodeLinkedList;

public interface LinkedListInterface<T>{

    void pushFront(T key);
    T topFront();
    T popFront();
    void pushBack(T key);
    T topBack();
    T popBack();
    boolean find(T key);
    void erase(T key);
    boolean isEmpty();
    void addBefore(NodeLinkedList<T> node, T key);
    void addAfter(NodeLinkedList<T> node, T key);
    String print();

}
