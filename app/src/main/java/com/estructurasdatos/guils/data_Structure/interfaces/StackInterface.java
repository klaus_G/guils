package com.estructurasdatos.guils.data_Structure.interfaces;
public interface StackInterface<T> {
    void push(T key);
    T top();
    T pop();
    boolean isEmpty();
    String print();
}
