package com.estructurasdatos.guils.data_Structure.interfaces;

import com.estructurasdatos.guils.data_Structure.DynamicArray;
import com.estructurasdatos.guils.data_Structure.NodeTree;

public interface BinaryTreeInterface<T> {
    void insert(T key);
    void delete(T key);
    NodeTree<T> find(T key);
    NodeTree<T> next(NodeTree<T> n);
    DynamicArray<NodeTree<T>> rangeSearch(T x, T y);
}
