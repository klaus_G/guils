package com.estructurasdatos.guils.data_Structure;

import com.estructurasdatos.guils.data_Structure.interfaces.BinaryHeapInterface;
import com.estructurasdatos.guils.model.News;
import com.estructurasdatos.guils.util.TimeCount;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.firestore.DocumentSnapshot;

public class BinaryHeap<T> implements BinaryHeapInterface<T> {

    public DynamicArray<T> elements;
    public int position;


    public BinaryHeap() {
        elements=new DynamicArray<>();
        position=0;
    }

    public BinaryHeap(DynamicArray<T> elements,int position) {
        this.elements = elements;
        this.position = position;
    }

    public DynamicArray<T> autoSort(){
        DynamicArray<T> aux= this.elements;

        return this.heapSort(aux.getArray());
    }

    @Override
    public T getMax() {
        return this.elements.get(0);
    }

    @Override
    public void insert(T key) {
        this.elements.add(this.position,key);
        siftUp(position);
        this.position++;
    }

    @Override
    public void siftUp(int i) {
        while (i!=0 && getInt(i)>getInt(parent(i))){
            T aux = this.elements.get(i);
            this.elements.set(i,this.elements.get(parent(i)));
            this.elements.set(parent(i),aux);
            i=parent(i);
        }
    }

    @Override
    public void siftDown(int i) {
        int maxIndex = i;

        int left = leftChild(i);
        if (left < this.elements.getPosition()+1 && getInt(left) > getInt(maxIndex)) {
            maxIndex = left;
        }

        int right = rightChild(i);
        if (right < this.elements.getPosition()+1 && getInt(right) > getInt(maxIndex)) {
            maxIndex = rightChild(i);
        }
        if (i != maxIndex) {
            T aux = this.elements.get(i);
            this.elements.set(i,this.elements.get(maxIndex));
            this.elements.set(maxIndex,aux);
            siftDown(maxIndex);
        }
    }

    @Override
    public T extractMax() {
        T aux = this.elements.get(0);
        this.elements.set(0,this.elements.get(this.position-1));
        this.elements.remove(this.position-1);
        this.position--;
        siftDown(0);
        return aux;
    }

    @Override
    public T remove(int i) {
        changePriority(Integer.MAX_VALUE,i);
        siftUp(i);
        return extractMax();
    }

    @Override
    public void changePriority(int priority, int position) {
        DynamicArray<T> data = this.elements;
        if(data.get(position).getClass().equals(Integer.class)){
            data.set(position, (T)(Object)priority);
        }else{

        }
    }

    @Override
    public DynamicArray<T> heapSort(T[] array) {
        TimeCount timeCount=new TimeCount();
        timeCount.settInicio(System.currentTimeMillis());

        buildHeap(array);
        DynamicArray<T> arr = new DynamicArray<>();

        for (int i = 0; i < array.length-1; i++) {
            T max = this.extractMax();
            arr.add(i,max);
        }

        timeCount.settFinal(System.currentTimeMillis());

        //Log.v("Count buildHeap",arr.getPosition()+": "+timeCount.geteEj());
        return arr;
    }

    @Override
    public void partialSorting(T[] array, int k) {

    }

    @Override
    public void buildHeap(T[] array) {
        this.elements.setArray(array);
        this.position = array.length-1;

        for (int i = parent(this.position); i >= 0; i--) {
            siftDown(i);
        }
    }

    public void reset(){
        elements=new DynamicArray<>();
        position=0;
    }


    public boolean isEmpty(){
        return elements.getPosition()==0;
    }

    private int parent(int i) {
        return (i - 1) >> 1;
    }

    private int leftChild(int i) {
        return (i << 1) + 1;
    }

    private int rightChild(int i) {
        return (i << 1) + 2;
    }

    public int getInt(int key){
        DynamicArray<T> data = this.elements;

        try{

        if(data.get(key)instanceof Integer) {
            return (Integer) data.get(key);
        }else if(data.get(key)instanceof News){
            return  ((News)data.get(key)).getPriority();
        }else if(data.get(key) instanceof DocumentSnapshot){
            DocumentSnapshot ds=(DocumentSnapshot)data.get(key);
            return  (ds.toObject(News.class)).getPriority();
        }else if(data.get(key)instanceof DataSnapshot){

            try {
                News news=((DataSnapshot) data.get(key)).getValue(News.class);
                return news.getPriority();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }



}
