package com.estructurasdatos.guils.data_Structure;

import com.estructurasdatos.guils.data_Structure.interfaces.StackInterface;

public class Stack<T> implements StackInterface<T> {

    private LinkedList<T> list;

    public Stack() {
        this.list = new LinkedList<>();
    }

    @Override
    public void push(T key) {
        list.pushBack(key);
    }

    @Override
    public T top() {
        return list.topBack();
    }

    @Override
    public T pop() {
        return list.popBack();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public String print() {
        return null;
    }
}
