package com.estructurasdatos.guils.data_Structure.Iterators;

import com.estructurasdatos.guils.data_Structure.LinkedList;
import com.estructurasdatos.guils.data_Structure.NodeLinkedList;

import java.util.Iterator;

public class ListIterator<T> implements Iterator<T> {
    private NodeLinkedList<T> node;

    public ListIterator(LinkedList<T> list) {
        this.node=list.getFirstNode();
    }

    @Override
    public boolean hasNext() {
        return node!=null;
    }

    @Override
    public T next() {
        T ob = node.getElement();
        node=node.getNext();
        return ob;
    }

}