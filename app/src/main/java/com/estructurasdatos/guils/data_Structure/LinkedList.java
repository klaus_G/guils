package com.estructurasdatos.guils.data_Structure;

import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.estructurasdatos.guils.data_Structure.Iterators.ListIterator;
import com.estructurasdatos.guils.data_Structure.interfaces.LinkedListInterface;

import java.util.Iterator;
import java.util.Objects;

public class LinkedList<T> implements LinkedListInterface<T>, Iterable<T>{

    protected NodeLinkedList<T> firstNode;
    protected NodeLinkedList<T> lastNode;
    protected int size;

    public LinkedList() {
        this.firstNode=null;
        this.lastNode=null;
        this.size=0;
    }

    public int size(){
        return size;
    }

    public NodeLinkedList<T> getFirstNode(){
        return this.firstNode;
    }

    @Override
    public void pushFront(T key) {
        NodeLinkedList<T> node = new NodeLinkedList<>(key);

        if(this.isEmpty()){
            firstNode=node;
            lastNode=node;
        }else{
            node.next=firstNode;
            firstNode=node;
        }
        size++;
    }

    @Override
    public T topFront() {
        if(this.isEmpty()) {
            return null;
        }else{
            return firstNode.element;
        }
    }

    @Override
    public T popFront() {
        if(this.isEmpty()){
            return null;
        }else{
            NodeLinkedList<T> node = firstNode;
            firstNode=firstNode.next;
            size--;
            return node.element;
        }

    }

    @Override
    public void pushBack(T key) {
        NodeLinkedList<T> node = new NodeLinkedList<>(key);

        if(this.isEmpty()){
            firstNode=node;
            lastNode=node;
        }else{
            lastNode.next=node;
            lastNode=node;
        }
        size++;

    }

    @Override
    public T topBack() {
        if(this.isEmpty()) {
            return null;
        }else{
            return lastNode.element;
        }
    }

    @Override
    public T popBack() {
        if(this.isEmpty()) {
            return null;
        }else{
            NodeLinkedList<T> n = firstNode;
            while (n.next.next != null) {
                n = n.next;
            }
            NodeLinkedList<T> s = lastNode;
            lastNode=n.next;
            n.next=null;
            size--;
            return s.element;
        }
    }

    @Override
    public boolean find(T key) {
        if(this.isEmpty()) {
            return false;
        }else{
            NodeLinkedList<T> node = firstNode;
            while (node.next != null){
                if (node.element==key){
                    return true;
                }
                node=node.next;
            }
            return false;
        }
    }

    @Override
    public void erase(T key) {
        NodeLinkedList<T> node = firstNode;
        if (!this.isEmpty()){
            while (node.next != null){
                if (node.next.element==key){
                    if(node.next.next!=null){
                        node.next=node.next.next;
                    }else{
                        node.next=null;
                    }
                    size--;
                    break;
                }
                node=node.next;
            }
        }

    }

    @Override
    public boolean isEmpty() {
        return size==0;
    }

    @Override
    public void addBefore(NodeLinkedList<T> node, T key) {
        if (this.isEmpty()){

        }else {
            NodeLinkedList<T> newNode = new NodeLinkedList<>(key);
            NodeLinkedList<T> n = firstNode;
            while (n.next != node) {
                n = n.next;
            }
            newNode.next = node;
            n.next = newNode;
        }
    }

    @Override
    public void addAfter(NodeLinkedList<T> node, T key) {
        if (this.isEmpty()){
            //no se que poner aquí todavia
        }else {
            NodeLinkedList<T> newNode = new NodeLinkedList<>(key);
            newNode.next = node.next;
            node.next = newNode;
        }
    }

    @Override
    public String print() {
        return null;
    }

    @Override
    public Iterator<T> iterator() {
        return new ListIterator<>(this);
    }
}

