package com.estructurasdatos.guils.presenter;

import com.estructurasdatos.guils.interfaces.CRUDInterfaceRT;
import com.estructurasdatos.guils.model.CRUD_RT.NewsFirebase;
import com.estructurasdatos.guils.model.News;

public class NewsPresenterRT {

    private NewsFirebase newsFirebase;

    public NewsPresenterRT() {
        newsFirebase=new NewsFirebase();
    }


    public void element(String id, CRUDInterfaceRT.ElementListener<News> listener) {


    }

    public void checkLike(String userId, String newsId, final CRUDInterfaceRT.ElementListener<Integer> listener){
        newsFirebase.checkLike(userId,newsId,listener);
    }

    public void add(News news) {
        newsFirebase.add(news);
    }


    public void addLike(String userId,String newsId) {

        newsFirebase.addLike(userId,newsId);

    }

    public void removeLike(String userId,String newsId) {

        newsFirebase.addLike(userId,newsId);

    }

    public String newElement() {
        return null;
    }


    public void erase(String id) {

    }


    public void update(String id, News news) {
        newsFirebase.update(id,news);

    }

    public void update(String userId,String newsId, News news) {
        newsFirebase.update(userId,newsId,news);
    }

    public void size(CRUDInterfaceRT.SizeListener listener) {

    }

}
