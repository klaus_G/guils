package com.estructurasdatos.guils.presenter;

import com.estructurasdatos.guils.interfaces.CRUDInterfaceRT;
import com.estructurasdatos.guils.model.CRUD_RT.DriverFirebase;
import com.estructurasdatos.guils.model.Driver;

public class DriverPresenterRT {

    private DriverFirebase driverFirebase;

    public DriverPresenterRT() {
        driverFirebase = new DriverFirebase();
    }


    public void element(String id, final CRUDInterfaceRT.ElementListener<Driver> listener) {
        driverFirebase.element(id,listener);
    }


    public void add(Driver driver) {
        throw  new UnsupportedOperationException("Not supported yet");
    }

    public void add(String id,Driver driver) {
        driverFirebase.add(id,driver);
    }


    public String newElement() {
        return driverFirebase.newElement();
    }

    public void erase(String id) {
        driverFirebase.erase(id);
    }

    public void update(String id, Driver driver) {
        driverFirebase.update(id,driver);
    }

    public void size(final CRUDInterfaceRT.SizeListener listener) {
        driverFirebase.size(listener);
    }
}
