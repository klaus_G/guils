package com.estructurasdatos.guils.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import com.estructurasdatos.guils.interfaces.CRUDInterface;
import com.estructurasdatos.guils.model.CRUD.AllUserListener;
import com.estructurasdatos.guils.model.CRUD.UsersFirestore;
import com.estructurasdatos.guils.model.User;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class UserPresenter{

    private final String TAG = "UserPresenter.class";
    private UsersFirestore usersFirestore;
    GoogleSignInClient mGoogleSignInClient;
    Context context;

    public UserPresenter() {
        this.usersFirestore = new UsersFirestore();
    }

    public UserPresenter(Context context, String token) {
        this.usersFirestore = new UsersFirestore();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(token)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(context,gso);
        this.context=context;
    }

    public void element(String id, final CRUDInterface.ElementListener<User> listener) {
       usersFirestore.element(id,listener);
    }

    public void add(FirebaseUser firebaseUser) {
        usersFirestore.update(firebaseUser.getUid(),new User(firebaseUser.getDisplayName(),firebaseUser.getEmail(),firebaseUser.getPhoneNumber(),null,0));
    }

    public void add(User user) {
        usersFirestore.add(user);
    }


    public void getUsers(AllUserListener listener){
        usersFirestore.getUsers(listener);
    }

    public void newElement() {
        usersFirestore.newElement();
    }

    public void erase(FirebaseUser firebaseUser) {

        usersFirestore.erase(firebaseUser.getUid());

        firebaseUser.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Log.d(TAG,"User account deleted");
                }
            }

        });

        mGoogleSignInClient.signOut()
                .addOnCompleteListener((Activity) context, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            Log.d(TAG,"Sign out from Google");
                        }
                    }
                });
    }

    public void erase(String id) {
        usersFirestore.erase(id);

        usersFirestore.element(id, new CRUDInterface.ElementListener<User>() {
            @Override
            public void onReaction(User user) {
                FirebaseAuth.getInstance()
                            .getCurrentUser()
                            .delete()
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()){
                                        Log.d(TAG,"User account deleted");
                                    }
                                }
                            });
                signOutFromGoogle();
            }
        });
    }

    public void update(String id, User user) {
        usersFirestore.update(id,user);
    }

    public void update(FirebaseUser firebaseUser, User user) {
        usersFirestore.update(firebaseUser.getUid(),user);
    }

    public void size(CRUDInterface.SizeListener listener) {
        usersFirestore.size(listener);
    }

    public Intent signIn() {
        return mGoogleSignInClient.getSignInIntent();
    }

    public void signOut(){
        FirebaseAuth.getInstance().signOut();
        signOutFromGoogle();
    }

    public void signOutFromGoogle() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener((Activity) this.context, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            Log.d(TAG,"Sign out from Google");
                        }
                    }
                });
    }

}
