package com.estructurasdatos.guils.presenter;

import com.estructurasdatos.guils.interfaces.CRUDInterface;
import com.estructurasdatos.guils.model.CRUD.NewsFirestore;
import com.estructurasdatos.guils.model.News;

public class NewsPresenter {
    private NewsFirestore newsFirestore;

    public NewsPresenter() {
        newsFirestore=new NewsFirestore();
    }

    public void element(String id, final CRUDInterface.ElementListener<News> listener) {
        newsFirestore.element(id,listener);
    }

    public void add(News news,String id) {
        newsFirestore.add(news,id);
    }

    public void add(News news) {
        newsFirestore.add(news);
    }

    public void newElement(){
        newsFirestore.newElement();
    }

    public void erase(String id) {
        newsFirestore.erase(id);
    }

    public void erase(String idNews,String idUser){newsFirestore.erase(idNews,idUser);}


        public void update(String id, News news) {
        newsFirestore.update(id,news);
    }

    public void size(final CRUDInterface.SizeListener listener) {
        newsFirestore.size(listener);
    }

}
