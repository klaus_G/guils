package com.estructurasdatos.guils.presenter;

import android.util.Log;

import androidx.annotation.NonNull;

import com.estructurasdatos.guils.interfaces.CRUDInterface;
import com.estructurasdatos.guils.model.CRUD.ChatsFirestore;
import com.estructurasdatos.guils.model.Chat;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

public class ChatPresenter {
    private ChatsFirestore chatsFirestore;

    public ChatPresenter() {
        chatsFirestore=new ChatsFirestore();
    }


    public void element(String id, final CRUDInterface.ElementListener<Chat> listener) {
        chatsFirestore.element(id,listener);
    }

    public void add(Chat chat) {
        chatsFirestore.add(chat);
    }

    public void newElement(){
        chatsFirestore.newElement();
    }

    public void erase(String id) {
        chatsFirestore.erase(id);
    }

    public void update(String id, Chat chat) {
        chatsFirestore.update(id,chat);
    }

    public void size(final CRUDInterface.SizeListener listener) {
        chatsFirestore.size(listener);
    }
}
