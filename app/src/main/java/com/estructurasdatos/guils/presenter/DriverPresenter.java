package com.estructurasdatos.guils.presenter;

import com.estructurasdatos.guils.interfaces.CRUDInterface;
import com.estructurasdatos.guils.model.CRUD.DriversFirestore;
import com.estructurasdatos.guils.model.Driver;

public class DriverPresenter {
    private DriversFirestore driversFirestore;

    public DriverPresenter() {
        driversFirestore=new DriversFirestore();
    }

    public void element(String id, final CRUDInterface.ElementListener<Driver> listener) {
        driversFirestore.element(id,listener);
    }

    public void add(Driver driver) {
        driversFirestore.add(driver);
    }

    public void newElement(){
        driversFirestore.newElement();
    }

    public void erase(String id) {
        driversFirestore.erase(id);
    }

    public void update(String id, Driver driver) {
        driversFirestore.update(id,driver);
    }

    public void size(final CRUDInterface.SizeListener listener) {
        driversFirestore.size(listener);
    }

}
