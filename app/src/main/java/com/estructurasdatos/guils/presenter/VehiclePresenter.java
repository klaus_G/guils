package com.estructurasdatos.guils.presenter;

import com.estructurasdatos.guils.interfaces.CRUDInterface;
import com.estructurasdatos.guils.model.CRUD_RT.VehicleFirebase;
import com.estructurasdatos.guils.model.Vehicle;

public class VehiclePresenter {
    private VehicleFirebase vehiclesFirestore;

    public VehiclePresenter() {
        vehiclesFirestore=new VehicleFirebase();
    }

    public void element(String id, final CRUDInterface.ElementListener<Vehicle> listener) {

    }

    public void add(String id,Vehicle vehicle) {
        vehiclesFirestore.add(id,vehicle);
    }

    public void newElement(){
        vehiclesFirestore.newElement();
    }

    public void erase(String id) {
        vehiclesFirestore.erase(id);
    }

    public void update(String id, Vehicle vehicle) {
        vehiclesFirestore.update(id,vehicle);
    }

    public void size(final CRUDInterface.SizeListener listener) {
       // vehiclesFirestore.size(listener);
    }

}
