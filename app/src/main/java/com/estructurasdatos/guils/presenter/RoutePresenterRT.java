package com.estructurasdatos.guils.presenter;

import com.estructurasdatos.guils.interfaces.CRUDInterfaceRT;
import com.estructurasdatos.guils.model.CRUD_RT.RoutesFirebase;
import com.estructurasdatos.guils.model.Route;

public class RoutePresenterRT {

    RoutesFirebase routesFirebase;

    public RoutePresenterRT() {
        routesFirebase = new RoutesFirebase();
    }

    public void element(String id, final CRUDInterfaceRT.ElementListener<Route> listener) {
       routesFirebase.element(id,listener);
    }


    public void add(Route route) {
        routesFirebase.add(route);
    }

    public void add(String id, Route route){
        routesFirebase.add(id, route);
    }

    public String newElement() {
        return routesFirebase.newElement();
    }

    public void erase(String id) {
        routesFirebase.erase(id);
    }

    public void update(String id, Route route) {
        routesFirebase.update(id,route);
    }

    public void size(final CRUDInterfaceRT.SizeListener listener) {
        routesFirebase.size(listener);
    }

}
