package com.estructurasdatos.guils.presenter;

import com.estructurasdatos.guils.interfaces.LoginPresenterInterface;
import com.google.firebase.auth.FirebaseUser;

public class LoginPresenter implements LoginPresenterInterface {
    @Override
    public boolean updateUI(FirebaseUser firebaseUser) {
        return firebaseUser!=null;
    }
}
