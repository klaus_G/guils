package com.estructurasdatos.guils.presenter;

import com.estructurasdatos.guils.interfaces.CRUDInterface;
import com.estructurasdatos.guils.model.CRUD.MessagesFirestore;
import com.estructurasdatos.guils.model.Message;
public class MessagePresenter {
    private MessagesFirestore messagesFirestore;

    public MessagePresenter() {
    }

    public void element(String id, final CRUDInterface.ElementListener<Message> listener) {
        messagesFirestore.element(id,listener);
    }

    public void add(Message message) {
        messagesFirestore.add(message);
    }

    public void newElement(){
        messagesFirestore.newElement();
    }

    public void erase(String id) {
        messagesFirestore.erase(id);
    }

    public void update(String id, Message message) {
        messagesFirestore.update(id,message);
    }

    public void size(final CRUDInterface.SizeListener listener) {
        messagesFirestore.size(listener);
    }

}
