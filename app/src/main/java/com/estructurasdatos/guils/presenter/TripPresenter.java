package com.estructurasdatos.guils.presenter;

import com.estructurasdatos.guils.interfaces.CRUDInterface;
import com.estructurasdatos.guils.model.Trip;

public class TripPresenter {
    private TripPresenter tripPresenter;

    public TripPresenter() {
    }

    public void element(String id, final CRUDInterface.ElementListener<Trip> listener) {
        tripPresenter.element(id,listener);
    }

    public void add(Trip trip) {
        tripPresenter.add(trip);
    }

    public void newElement(){
        tripPresenter.newElement();
    }

    public void erase(String id) {
        tripPresenter.erase(id);

    }

    public void update(String id, Trip trip) {
        tripPresenter.update(id,trip);
    }

    public void size(final CRUDInterface.SizeListener listener) {
        tripPresenter.size(listener);
    }

}
