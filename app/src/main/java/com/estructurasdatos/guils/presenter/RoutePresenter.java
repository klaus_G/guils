package com.estructurasdatos.guils.presenter;

import com.estructurasdatos.guils.interfaces.CRUDInterface;
import com.estructurasdatos.guils.model.Route;

public class RoutePresenter {
    private RoutePresenter routePresenter;

    public RoutePresenter() {
    }

    public void element(String id, final CRUDInterface.ElementListener<Route> listener) {
        routePresenter.element(id,listener);
    }

    public void add(Route route) {
        routePresenter.add(route);
    }

    public void newElement(){
        routePresenter.newElement();
    }

    public void erase(String id) {
        routePresenter.erase(id);
    }

    public void update(String id, Route route) {
        routePresenter.update(id,route);
    }

    public void size(final CRUDInterface.SizeListener listener) {
        routePresenter.size(listener);
    }

}
