package com.estructurasdatos.guils.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.data_Structure.BinaryHeap;
import com.estructurasdatos.guils.data_Structure.DynamicArray;
import com.estructurasdatos.guils.model.News;
import com.estructurasdatos.guils.presenter.NewsPresenter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

public class AdapterNewsFirestore
                                extends RecyclerView.Adapter<AdapterNewsFirestore.NewsViewHolder>
                                implements EventListener<QuerySnapshot> {

    public static final String TAG = "AdapterNewsFirestore";
    private Query query;
    private DynamicArray<News> items;
    private ListenerRegistration registration;
    private LayoutInflater inflater;
    private View.OnClickListener onClickListener;
    private Context context;
    private FirebaseAuth mAuth;
    private NewsPresenter newsPresenter;
    private BinaryHeap<News> maxNews;

    public AdapterNewsFirestore(Context context,Query query) {
        mAuth=FirebaseAuth.getInstance();
        maxNews= new BinaryHeap<>();
        items = new DynamicArray<>();
        this.query = query;
        this.context=context;
        inflater=(LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context=context;
        newsPresenter=new NewsPresenter();

    }

    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= inflater.inflate(R.layout.news_rows_recycler,parent,false);
        v.setOnClickListener(onClickListener);
        return new NewsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder holder, int position) {
        News news=null;
        try {
           news = maxNews.extractMax();
        }catch (Exception e){

        }
        customizeView(holder,news);
    }

    private void customizeView(final NewsViewHolder holder, final News news) {



                holder.txt_nombre.setText((news.getUser()!=null)?news.getUser():"");
                holder.txt_contenido.setText("P: " + news.getPriority() + " " + ((news.getText()!=null)?news.getText():""));
                holder.txt_likes.setText(String.valueOf(news.getLikes()));

        final String key = (news.getDate()!=null)?news.getDate().toString().replace(" ","") + " " + news.getUserId():"";

            holder.btn_likes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(holder.btn_likes.getTag().toString().equalsIgnoreCase("unlike")) {
                        String uri = "@android:drawable/btn_star_big_on";
                        Drawable image = ContextCompat.getDrawable(context, context.getResources().getIdentifier(uri, null, context.getPackageName()));
                        holder.btn_likes.setBackground(image);
                        holder.btn_likes.setTag("like");
                    }else if(holder.btn_likes.getTag().toString().equalsIgnoreCase("like")){
                        String uri = "@android:drawable/btn_star_big_off";
                        Drawable image = ContextCompat.getDrawable(context, context.getResources().getIdentifier(uri, null, context.getPackageName()));
                        newsPresenter.erase(key,mAuth.getCurrentUser().getUid());
                        holder.btn_likes.setBackground(image);
                        holder.btn_likes.setTag("unlike");
                    }
                }
            });

    }

    public News getItem(int position){

        News news = maxNews.extractMax();
        return news;
    }

    @Override
    public int getItemCount() {
        return maxNews.elements.getPosition();
    }

    public void setOnClickListener(View.OnClickListener onClickListener){
        this.onClickListener = onClickListener;
    }

    public void startListening(){
        maxNews=new BinaryHeap<>();
        items=new DynamicArray<>();
        registration = query.addSnapshotListener(this);
    }

    public void stopListening(){
        registration.remove();
    }

    @Override
    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {

        long tInicio;
        long tFinal;
        long tEj;

        if(e != null){
            Log.e(TAG,"error al recibir evento",e);
            return;
        }
        maxNews.reset();
        tInicio=System.currentTimeMillis();
        for(DocumentChange x : queryDocumentSnapshots.getDocumentChanges()){
            maxNews.insert(x.getDocument().toObject(News.class));
            notifyDataSetChanged();
        }
        tFinal=System.currentTimeMillis();
        tEj=tFinal-tInicio;
        Toast.makeText(context,String.valueOf(tEj),Toast.LENGTH_SHORT).show();

    }

    public class NewsViewHolder extends RecyclerView.ViewHolder{

        TextView txt_nombre;
        TextView txt_contenido;
        TextView txt_likes;
        Button btn_likes;

        public NewsViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_nombre=itemView.findViewById(R.id.txt_news_recycler_nombre);
            txt_contenido=itemView.findViewById(R.id.txt_news_recycler_contenido);
            txt_likes=itemView.findViewById(R.id.txt_number_likes);
            btn_likes=itemView.findViewById(R.id.btn_like_new);
        }
    }

}
