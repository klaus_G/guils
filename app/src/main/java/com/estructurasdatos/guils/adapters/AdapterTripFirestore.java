package com.estructurasdatos.guils.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.data_Structure.DynamicArray;
import com.estructurasdatos.guils.model.Trip;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import static java.lang.Math.abs;
import static java.lang.Math.min;

public class AdapterTripFirestore extends RecyclerView.Adapter<AdapterTripFirestore.TripViewHolder>
        implements EventListener<QuerySnapshot> {

    public static final String TAG = "AdapterTripFirestore";
    private Query query;
    private DynamicArray<DocumentSnapshot> items;
    private ListenerRegistration registration;
    private LayoutInflater inflater;
    private View.OnClickListener onClickListener;

    public AdapterTripFirestore(Context context, Query query) {
        items=new DynamicArray<>();
        this.query = query;
        inflater=(LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public TripViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= inflater.inflate(R.layout.trips_rows_recycler,null);
        v.setOnClickListener(onClickListener);
        return new TripViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TripViewHolder holder, int position) {
        Trip trip = getItem(position);
        customizeView(holder,trip);
    }

    private void customizeView(TripViewHolder holder, Trip trip) {
        //holder.txt_ruta_contenido.setText(trip.getRoute().getDriver());
        //holder.txt_conductor_contenido.setText(String.valueOf(trip.getRoute().getLat_st())+String.valueOf(trip.getRoute().getLong_st())+String.valueOf(trip.getRoute().getLat_fin())+String.valueOf(trip.getRoute().getLong_fin()));
        //holder.txt_cupos_nombre_contenido.setText(trip.getPassengers());
        //holder.txt_hora_contenido.setText(trip.getTime().toString());
    }

    public Trip getItem(int position){
        return items.get(position).toObject(Trip.class);
    }

    public String getKey(int position){
        return items.get(position).getId();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setOnClickListener(View.OnClickListener onClickListener){
        this.onClickListener = onClickListener;
    }

    public void startListening(){
        items = new DynamicArray<>();
        registration = query.addSnapshotListener(this);
    }

    public void stopListening(){
        registration.remove();
    }

    @Override
    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {

        if(e != null){
            Log.e(TAG,"error al recibir evento",e);
            return;
        }

        for (DocumentChange dc:queryDocumentSnapshots.getDocumentChanges()){
            int position=dc.getNewIndex();
            int oldPosition=dc.getOldIndex();
            switch (dc.getType()){
                case ADDED:
                    items.add(position,dc.getDocument());
                    notifyItemChanged(position);
                    Log.w(TAG,"ADDED",e);
                    break;
                case REMOVED:
                    items.remove(oldPosition);
                    notifyItemRemoved(oldPosition);
                    Log.w(TAG,"REMOVED",e);
                    break;
                case MODIFIED:
                    items.remove(oldPosition);
                    items.set(position,dc.getDocument());
                    notifyItemRangeChanged(min(position,oldPosition),abs(position-oldPosition)+1);
                    Log.w(TAG,"MODIFIED",e);
                    break;
                default:
                    Log.w(TAG,"Tipo de cambio desconocido",e);
            }
        }

    }

    public class TripViewHolder extends RecyclerView.ViewHolder{
        TextView txt_ruta_contenido;
        TextView txt_conductor_contenido;
        TextView txt_cupos_nombre_contenido;
        TextView txt_hora_contenido;

        public TripViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_ruta_contenido=itemView.findViewById(R.id.txt_ruta_contenido);
            txt_conductor_contenido=itemView.findViewById(R.id.txt_conductor_contenido);
            txt_cupos_nombre_contenido=itemView.findViewById(R.id.txt_cupos_nombre_contenido);
            txt_hora_contenido=itemView.findViewById(R.id.txt_hora_contenido);

        }
    }
}
