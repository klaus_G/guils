package com.estructurasdatos.guils.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.data_Structure.DynamicArray;
import com.estructurasdatos.guils.model.Message;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import static java.lang.Math.abs;
import static java.lang.Math.min;

public class AdapterMessageFirestore extends RecyclerView.Adapter<AdapterMessageFirestore.MessageViewHolder>
        implements EventListener<QuerySnapshot> {

    public static final String TAG = "AdapterMessageFirestore";
    private Query query;
    private DynamicArray<DocumentSnapshot> items;
    private ListenerRegistration registration;
    private LayoutInflater inflater;
    private View.OnClickListener onClickListener;

    public AdapterMessageFirestore(Context context, Query query) {
        items=new DynamicArray<>();
        this.query = query;
        inflater=(LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= inflater.inflate(R.layout.news_rows_recycler,null);
        v.setOnClickListener(onClickListener);
        return new MessageViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder holder, int position) {
        Message message = getItem(position);
        customizeView(holder,message);
    }

    private void customizeView(MessageViewHolder holder, Message message) {
    }

    public Message getItem(int position){
        return items.get(position).toObject(Message.class);
    }

    public String getKey(int position){
        return items.get(position).getId();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setOnClickListener(View.OnClickListener onClickListener){
        this.onClickListener = onClickListener;
    }

    public void startListening(){
        items = new DynamicArray<>();
        registration = query.addSnapshotListener(this);
    }

    public void stopListening(){
        registration.remove();
    }

    @Override
    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {

        if(e != null){
            Log.e(TAG,"error al recibir evento",e);
            return;
        }

        for (DocumentChange dc:queryDocumentSnapshots.getDocumentChanges()){
            int position=dc.getNewIndex();
            int oldPosition=dc.getOldIndex();
            switch (dc.getType()){
                case ADDED:
                    items.add(position,dc.getDocument());
                    notifyItemChanged(position);
                    Log.w(TAG,"ADDED",e);
                    break;
                case REMOVED:
                    items.remove(oldPosition);
                    notifyItemRemoved(oldPosition);
                    Log.w(TAG,"REMOVED",e);
                    break;
                case MODIFIED:
                    items.remove(oldPosition);
                    items.set(position,dc.getDocument());
                    notifyItemRangeChanged(min(position,oldPosition),abs(position-oldPosition)+1);
                    Log.w(TAG,"MODIFIED",e);
                    break;
                default:
                    Log.w(TAG,"Tipo de cambio desconocido",e);
            }
        }

    }

    public class MessageViewHolder extends RecyclerView.ViewHolder{


        public MessageViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
