package com.estructurasdatos.guils.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.data_Structure.DynamicArray;
import com.estructurasdatos.guils.model.Route;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import static java.lang.Math.abs;
import static java.lang.Math.min;

public class AdapterRouteFirestore extends RecyclerView.Adapter<AdapterRouteFirestore.RouteViewHolder>
        implements EventListener<QuerySnapshot> {

    public static final String TAG = "AdapterRouteFirestore";
    private Query query;
    private DynamicArray<DocumentSnapshot> items;
    private ListenerRegistration registration;
    private LayoutInflater inflater;
    private View.OnClickListener onClickListener;

    public AdapterRouteFirestore(Context context, Query query) {
        items=new DynamicArray<>();
        this.query = query;
        inflater=(LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public RouteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= inflater.inflate(R.layout.news_rows_recycler,null);
        v.setOnClickListener(onClickListener);
        return new RouteViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RouteViewHolder holder, int position) {
        Route route = getItem(position);
        customizeView(holder,route);
    }

    private void customizeView(RouteViewHolder holder, Route route) {
        holder.txt_routes_description.setText(route.getPath());
        holder.txt_routes_origin.setText(route.getLat_st()+" "+route.getLong_st());
        holder.txt_routes_ending.setText(route.getLat_fin()+" "+route.getLong_fin());
    }

    public Route getItem(int position){
        return items.get(position).toObject(Route.class);
    }

    public String getKey(int position){
        return items.get(position).getId();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setOnClickListener(View.OnClickListener onClickListener){
        this.onClickListener = onClickListener;
    }

    public void startListening(){
        items = new DynamicArray<>();
        registration = query.addSnapshotListener(this);
    }

    public void stopListening(){
        registration.remove();
    }

    @Override
    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {

        if(e != null){
            Log.e(TAG,"error al recibir evento",e);
            return;
        }

        for (DocumentChange dc:queryDocumentSnapshots.getDocumentChanges()){
            int position=dc.getNewIndex();
            int oldPosition=dc.getOldIndex();
            switch (dc.getType()){
                case ADDED:
                    items.add(position,dc.getDocument());
                    notifyItemChanged(position);
                    Log.w(TAG,"ADDED",e);
                    break;
                case REMOVED:
                    items.remove(oldPosition);
                    notifyItemRemoved(oldPosition);
                    Log.w(TAG,"REMOVED",e);
                    break;
                case MODIFIED:
                    items.remove(oldPosition);
                    items.set(position,dc.getDocument());
                    notifyItemRangeChanged(min(position,oldPosition),abs(position-oldPosition)+1);
                    Log.w(TAG,"MODIFIED",e);
                    break;
                default:
                    Log.w(TAG,"Tipo de cambio desconocido",e);
            }
        }

    }

    public class RouteViewHolder extends RecyclerView.ViewHolder{

        TextView txt_routes_description;
        TextView txt_routes_origin;
        TextView txt_routes_ending;

        public RouteViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_routes_description=itemView.findViewById(R.id.txt_routes_description);
            txt_routes_origin=itemView.findViewById(R.id.txt_routes_origin);
            txt_routes_ending=itemView.findViewById(R.id.txt_routes_ending);

        }
    }
}
