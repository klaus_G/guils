package com.estructurasdatos.guils.interfaces;

import com.google.firebase.auth.FirebaseUser;

public interface MainViewInterface {
    void updateUI(FirebaseUser firebaseUser);
}
