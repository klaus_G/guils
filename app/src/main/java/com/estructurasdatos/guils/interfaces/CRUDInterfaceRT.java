package com.estructurasdatos.guils.interfaces;

public interface CRUDInterfaceRT<T>{

    interface ElementListener<T>{
        void onReaction(T t);
    }

    interface SizeListener{
        void onReaction(long size);
    }

    void element(String id, ElementListener<T> listener);
    void add(T t);
    String newElement();
    void erase(String id);
    void update(String id, T t);
    void size(SizeListener listener);
}
