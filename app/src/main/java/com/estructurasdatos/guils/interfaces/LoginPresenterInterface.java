package com.estructurasdatos.guils.interfaces;

import com.google.firebase.auth.FirebaseUser;

public interface LoginPresenterInterface {
    boolean updateUI(FirebaseUser firebaseUser);
}
