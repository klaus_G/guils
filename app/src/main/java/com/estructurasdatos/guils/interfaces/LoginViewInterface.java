package com.estructurasdatos.guils.interfaces;

import com.google.firebase.auth.FirebaseUser;

public interface LoginViewInterface {
    void updateUI(FirebaseUser firebaseUser);
    void userRegistration(FirebaseUser firebaseUser);
    void verUnal(FirebaseUser firebaseUser);
}
