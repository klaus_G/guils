package com.estructurasdatos.guils.adaptersRT;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.data_Structure.DynamicArray;
import com.estructurasdatos.guils.util.Interface.RecyclerViewOnItemClickListener;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder>{

    private DynamicArray<String> DynamicArray;
    private Context context;
    private RecyclerViewOnItemClickListener recyclerViewOnItemClickListener;

    public Adapter(Context context, DynamicArray<String> DynamicArray,RecyclerViewOnItemClickListener recyclerViewOnItemClickListener) {
        this.context=context;
        this.DynamicArray = DynamicArray;
        this.recyclerViewOnItemClickListener=recyclerViewOnItemClickListener;

    }

    @NonNull
    @Override
    public Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.test_int_layout,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter.ViewHolder holder, int position) {
        String integer= DynamicArray.get(position);
        customizeView(holder,integer,position);

    }

    private void customizeView(final Adapter.ViewHolder holder, final String integer,int position) {

        holder.textView.setText(integer);

    }



    @Override
    public int getItemCount() {
        return DynamicArray.getPosition();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView textView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView=itemView.findViewById(R.id.txt_qwerty);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            recyclerViewOnItemClickListener.onClick(v,getAdapterPosition());
        }
    }

}
