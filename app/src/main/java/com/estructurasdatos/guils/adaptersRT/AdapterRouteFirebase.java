package com.estructurasdatos.guils.adaptersRT;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.data_Structure.DynamicArray;
import com.estructurasdatos.guils.model.Route;
import com.estructurasdatos.guils.util.Interface.RecyclerViewOnItemClickListener;

public class AdapterRouteFirebase extends RecyclerView.Adapter<AdapterRouteFirebase.RouteViewHolder>{

    private DynamicArray<Route> routeDynamicArray;
    private Context context;
    private RecyclerViewOnItemClickListener recyclerViewOnItemClickListener;

    public AdapterRouteFirebase(Context context, DynamicArray<Route> routeDynamicArray,RecyclerViewOnItemClickListener recyclerViewOnItemClickListener) {
        this.context=context;
        this.routeDynamicArray = routeDynamicArray;
        this.recyclerViewOnItemClickListener=recyclerViewOnItemClickListener;

    }

    @NonNull
    @Override
    public AdapterRouteFirebase.RouteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.routes_recycler,parent,false);
        return new RouteViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterRouteFirebase.RouteViewHolder holder, int position) {
        Route route= routeDynamicArray.get(position);
        customizeView(holder,route);

    }

    private void customizeView(final AdapterRouteFirebase.RouteViewHolder holder, final Route route) {


        try {

            holder.txt_descripcion.setText(route.getPath());
            holder.txt_origen.setText(route.getLat_st()+" "+route.getLong_st());
            holder.txt_destino.setText(route.getLat_fin()+" "+route.getLong_fin());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    @Override
    public int getItemCount() {
        return routeDynamicArray.getPosition();
    }

    public class RouteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView txt_descripcion;
        TextView txt_origen;
        TextView txt_destino;

        public RouteViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_descripcion=itemView.findViewById(R.id.txt_routes_description);
            txt_origen= itemView.findViewById(R.id.txt_routes_origin);
            txt_destino=itemView.findViewById(R.id.txt_routes_ending);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            recyclerViewOnItemClickListener.onClick(v,getAdapterPosition());
        }
    }

}
