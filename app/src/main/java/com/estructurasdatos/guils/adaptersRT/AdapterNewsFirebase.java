package com.estructurasdatos.guils.adaptersRT;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.data_Structure.DynamicArray;
import com.estructurasdatos.guils.interfaces.CRUDInterfaceRT;
import com.estructurasdatos.guils.model.News;
import com.estructurasdatos.guils.presenter.NewsPresenterRT;
import com.estructurasdatos.guils.util.Interface.RecyclerViewOnItemClickListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;

public class AdapterNewsFirebase extends RecyclerView.Adapter<AdapterNewsFirebase.NewsViewHolder>{

    private DynamicArray<DataSnapshot> dsnapDynamicArray;
    private DynamicArray<News> newsDynamicArray;
    private Context context;
    private RecyclerViewOnItemClickListener recyclerViewOnItemClickListener;


    public AdapterNewsFirebase(Context context, DynamicArray<News> newsDynamicArray,DynamicArray<DataSnapshot> dsnapDynamicArray,RecyclerViewOnItemClickListener recyclerViewOnItemClickListener) {
        this.context=context;
        this.newsDynamicArray = newsDynamicArray;
        this.dsnapDynamicArray=dsnapDynamicArray;
        this.recyclerViewOnItemClickListener=recyclerViewOnItemClickListener;
    }

    @NonNull
    @Override
    public AdapterNewsFirebase.NewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.news_rows_recycler,parent,false);
        return new NewsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterNewsFirebase.NewsViewHolder holder, int position) {
        DataSnapshot dataSnapshot= dsnapDynamicArray.get(position);
        News news=newsDynamicArray.get(position);
        customizeView(holder,news,dataSnapshot);

    }

    private void customizeView(final NewsViewHolder holder, final News news, final DataSnapshot dataSnapshot) {

        final NewsPresenterRT newsPresenterRT=new NewsPresenterRT();

        if(news.getPriority()<0){
            news.setPriority(0);
        }

        try {




            newsPresenterRT.checkLike(FirebaseAuth.getInstance().getCurrentUser().getUid(), dataSnapshot.getKey(), new CRUDInterfaceRT.ElementListener<Integer>() {
                @Override
                public void onReaction(Integer integer) {

                    final FirebaseUser firebaseUser=FirebaseAuth.getInstance().getCurrentUser();

                    holder.txt_nombre.setText(news.getUser());
                    holder.txt_contenido.setText(news.getDate()+"\n"+"Prioridad: " + news.getPriority() + "\n" + ((news.getText() != null) ? news.getText() : ""));
                    holder.txt_likes.setText(String.valueOf(news.getLikes()));

                    /*if(integer==null){
                        String uri = "@android:drawable/btn_star_big_off";
                        Drawable image = ContextCompat.getDrawable(context, context.getResources().getIdentifier(uri, null, context.getPackageName()));
                        holder.btn_likes.setBackground(image);
                        holder.btn_likes.setTag("unlike");
                    }else if(integer==1){
                        String uri = "@android:drawable/btn_star_big_on";
                        Drawable image = ContextCompat.getDrawable(context, context.getResources().getIdentifier(uri, null, context.getPackageName()));
                        holder.btn_likes.setBackground(image);
                        holder.btn_likes.setTag("like");
                    }*/

                    holder.btn_likes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (holder.btn_likes.getTag().toString().equalsIgnoreCase("unlike")) {
                                String uri = "@android:drawable/btn_star_big_on";
                                Drawable image = ContextCompat.getDrawable(context, context.getResources().getIdentifier(uri, null, context.getPackageName()));
                                news.setElementLikesmap(firebaseUser.getUid(),1);
                                news.addLike();
                                newsPresenterRT.update(firebaseUser.getUid(),dataSnapshot.getKey(),news);
                                holder.btn_likes.setBackground(image);
                                holder.btn_likes.setTag("like");
                            }else if (holder.btn_likes.getTag().toString().equalsIgnoreCase("like")) {
                                String uri = "@android:drawable/btn_star_big_off";
                                Drawable image = ContextCompat.getDrawable(context, context.getResources().getIdentifier(uri, null, context.getPackageName()));
                                news.setElementLikesmap(firebaseUser.getUid(),0);
                                news.removeLike();
                                newsPresenterRT.update(firebaseUser.getUid(),dataSnapshot.getKey(),news);
                                holder.btn_likes.setBackground(image);
                                holder.btn_likes.setTag("unlike");
                            }
                        }
                    });

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    @Override
    public int getItemCount() {
        return newsDynamicArray.getPosition();
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txt_nombre;
        TextView txt_contenido;
        TextView txt_likes;
        Button btn_likes;

        public NewsViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_nombre=itemView.findViewById(R.id.txt_news_recycler_nombre);
            txt_contenido=itemView.findViewById(R.id.txt_news_recycler_contenido);
            txt_likes=itemView.findViewById(R.id.txt_number_likes);
            btn_likes=itemView.findViewById(R.id.btn_like_new);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            recyclerViewOnItemClickListener.onClick(v,getAdapterPosition());
        }
    }

}
