package com.estructurasdatos.guils.adaptersRT;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.data_Structure.DynamicArray;
import com.estructurasdatos.guils.model.Vehicle;
import com.estructurasdatos.guils.util.Interface.RecyclerViewOnItemClickListener;

public class AdapterVehicleFirebase extends RecyclerView.Adapter<AdapterVehicleFirebase.VehicleViewHolder>{

    private DynamicArray<Vehicle> vehicleDynamicArray;
    private Context context;
    private RecyclerViewOnItemClickListener recyclerViewOnItemClickListener;

    public AdapterVehicleFirebase(Context context, DynamicArray<Vehicle> vehicleDynamicArray, RecyclerViewOnItemClickListener recyclerViewOnItemClickListener) {
        this.context=context;
        this.vehicleDynamicArray = vehicleDynamicArray;
        this.recyclerViewOnItemClickListener=recyclerViewOnItemClickListener;

    }

    @NonNull
    @Override
    public AdapterVehicleFirebase.VehicleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.car_recycle,parent,false);
        return new VehicleViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterVehicleFirebase.VehicleViewHolder holder, int position) {
        Vehicle vehicle= vehicleDynamicArray.get(position);
        customizeView(holder,vehicle);

    }

    private void customizeView(final AdapterVehicleFirebase.VehicleViewHolder holder, final Vehicle vehicle) {


        try {
            holder.txt_type.setText(vehicle.getType());
            holder.txt_plate.setText(vehicle.getPlate());
            holder.txt_model.setText(vehicle.getModel());
            holder.txt_color.setText(vehicle.getColor());
            holder.txt_brand.setText(vehicle.getBrand());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    @Override
    public int getItemCount() {
        return vehicleDynamicArray.getPosition();
    }

    public class VehicleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView txt_type;
        TextView txt_brand;
        TextView txt_model;
        TextView txt_color;
        TextView txt_plate;


        public VehicleViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_brand=itemView.findViewById(R.id.txt_car_brand);
            txt_color=itemView.findViewById(R.id.txt_car_color);
            txt_model=itemView.findViewById(R.id.txt_car_model);
            txt_plate=itemView.findViewById(R.id.txt_car_plate);
            txt_type=itemView.findViewById(R.id.txt_car_type);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            recyclerViewOnItemClickListener.onClick(v,getAdapterPosition());
        }
    }

}
