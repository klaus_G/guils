package com.estructurasdatos.guils.adaptersRT;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.data_Structure.DynamicArray;
import com.estructurasdatos.guils.model.Trip;
import com.estructurasdatos.guils.util.Interface.RecyclerViewOnItemClickListener;

public class AdapterTripFirebase extends RecyclerView.Adapter<AdapterTripFirebase.TripViewHolder>{

    private DynamicArray<Trip> tripDynamicArray;
    private Context context;
    private RecyclerViewOnItemClickListener recyclerViewOnItemClickListener;

    public AdapterTripFirebase(Context context, DynamicArray<Trip> tripDynamicArray, RecyclerViewOnItemClickListener recyclerViewOnItemClickListener) {
        this.context=context;
        this.tripDynamicArray = tripDynamicArray;
        this.recyclerViewOnItemClickListener=recyclerViewOnItemClickListener;

    }

    @NonNull
    @Override
    public AdapterTripFirebase.TripViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.routes_recycler,parent,false);
        return new TripViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterTripFirebase.TripViewHolder holder, int position) {
        Trip trip= tripDynamicArray.get(position);
        customizeView(holder,trip);

    }

    private void customizeView(final AdapterTripFirebase.TripViewHolder holder, final Trip trip) {


        try {


        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    @Override
    public int getItemCount() {
        return tripDynamicArray.getPosition();
    }

    public class TripViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TripViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            recyclerViewOnItemClickListener.onClick(v,getAdapterPosition());
        }
    }

}
