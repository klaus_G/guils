package com.estructurasdatos.guils.view.ui.driver_section;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.estructurasdatos.guils.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class DriverSectionFragment extends Fragment {

    View view;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        view=inflater.inflate(R.layout.fragment_driver_section, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        BottomNavigationView navView = view.findViewById(R.id.nav_view);
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_d_current_trips, R.id.nav_d_add_route, R.id.nav_d_chat,R.id.nav_d_profile)
                .build();
        NavController navController = Navigation.findNavController((AppCompatActivity)getActivity(), R.id.nav_host_fragment_driver);
        NavigationUI.setupActionBarWithNavController((AppCompatActivity)getActivity(),navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

    }
}