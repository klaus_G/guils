package com.estructurasdatos.guils.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.interfaces.CRUDInterfaceRT;
import com.estructurasdatos.guils.model.Driver;
import com.estructurasdatos.guils.presenter.DriverPresenterRT;
import com.estructurasdatos.guils.presenter.UserPresenter;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivityView extends AppCompatActivity  {

    private AppBarConfiguration mAppBarConfiguration;
    private UserPresenter userPresenter;
    private boolean driver;
    private TextView nombre;
    private TextView email;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener listener;
    private DriverPresenterRT driverPresenterRT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userPresenter=new UserPresenter(MainActivityView.this,getString(R.string.default_web_client_id));
        mAuth=FirebaseAuth.getInstance();
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        driverPresenterRT=new DriverPresenterRT();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.

        View navHeader = navigationView.getHeaderView(0);


        nombre=navHeader.findViewById(R.id.txt_nombre_inicio);
        email=navHeader.findViewById(R.id.txt_email_inicio);

        FirebaseUser firebaseUser=mAuth.getCurrentUser();
        nombre.setText(firebaseUser.getDisplayName());
        email.setText(firebaseUser.getEmail());

        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_main_section, R.id.nav_profile, R.id.nav_driver_section, R.id.nav_singOut)
                .setDrawerLayout(drawer)
                .build();

        final NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                if(destination.getId()==R.id.nav_singOut){
                    Toast.makeText(MainActivityView.this,"Cerrando Sesión",Toast.LENGTH_LONG).show();
                    userPresenter.signOut();
                    updateUI();
                }

                if(destination.getId()==R.id.nav_driver_section){
                    driverPresenterRT.element(mAuth.getCurrentUser().getUid(), new CRUDInterfaceRT.ElementListener<Driver>() {
                        @Override
                        public void onReaction(Driver driver) {
                            if(driver != null){

                            }
                        }
                    });
                }
            }
        });
    }

    private void updateUI() {
        Intent intent = new Intent(MainActivityView.this,LoginActivityView.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }





    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }


}
