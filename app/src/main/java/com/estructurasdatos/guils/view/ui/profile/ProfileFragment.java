package com.estructurasdatos.guils.view.ui.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.interfaces.CRUDInterface;
import com.estructurasdatos.guils.model.User;
import com.estructurasdatos.guils.presenter.UserPresenter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ProfileFragment extends Fragment {

    private FirebaseAuth mAuth;
    private UserPresenter userPresenter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_profile, container, false);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final TextView txt_nombre_usuario = getActivity().findViewById(R.id.txt_nombre_usuario);
        final TextView txt_email_usuario = getActivity().findViewById(R.id.txt_email_usuario);
        final EditText etxt_phone=getActivity().findViewById(R.id.etxt_phone);
        final TextView txt_rating=getActivity().findViewById(R.id.txt_rating);
        final Button btn_acept=getActivity().findViewById(R.id.btn_acept);

        mAuth=FirebaseAuth.getInstance();
        final FirebaseUser firebaseUser=mAuth.getCurrentUser();
        userPresenter = new UserPresenter();
        userPresenter.element(firebaseUser.getUid(), new CRUDInterface.ElementListener<User>() {
            @Override
            public void onReaction(final User user) {
                if(user!=null){

                    txt_nombre_usuario.setText(user.getName());
                    txt_email_usuario.setText(user.getEmail());
                    etxt_phone.setText((user.getPhone()==null)?"No registrado":user.getPhone());
                    txt_rating.setText(String.valueOf(user.getRating()));

                }
            }
        });

        btn_acept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(btn_acept.getText().toString().equalsIgnoreCase("EDITAR")){
                    btn_acept.setText("ACEPTAR");
                    etxt_phone.setText("");
                    etxt_phone.setEnabled(true);
                }else if(btn_acept.getText().toString().equalsIgnoreCase("ACEPTAR")){
                    userPresenter.element(firebaseUser.getUid(), new CRUDInterface.ElementListener<User>() {
                        @Override
                        public void onReaction(final User user) {
                            if(user!=null){
                                    etxt_phone.setEnabled(false);
                                    user.setPhone(etxt_phone.getText().toString());
                                    userPresenter.update(firebaseUser.getUid(),user);
                                    btn_acept.setText("EDITAR");
                            }
                        }
                    });

                }
            }
        });





    }

}
