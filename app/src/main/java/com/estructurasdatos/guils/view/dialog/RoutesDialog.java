package com.estructurasdatos.guils.view.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.fragment.app.DialogFragment;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.model.Route;
import com.estructurasdatos.guils.presenter.RoutePresenterRT;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
public class RoutesDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return createRoutesDialog();
    }

    public AlertDialog createRoutesDialog(){

        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v=inflater.inflate(R.layout.layout_create_routes,null);

        builder.setView(v);

        final EditText edtxt_origin=v.findViewById(R.id.ed_routes_origin);
        final EditText edtxt_ending=v.findViewById(R.id.ed_routes_ending);
        final EditText edtxt_description=v.findViewById(R.id.ed_routes_description);
        builder.setTitle("Agregar Ruta")
                .setPositiveButton("Agregar",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                final RoutePresenterRT routePresenterRT= new RoutePresenterRT();
                                final FirebaseUser firebaseUser= FirebaseAuth.getInstance().getCurrentUser();

                                String[] origin = String.valueOf(edtxt_origin.getText()).split("/");
                                String[] ending = String.valueOf(edtxt_ending.getText()).split("/");

                                Route route = new Route(String.valueOf(edtxt_description.getText()),Integer.parseInt(origin[0]),Integer.parseInt(origin[1]),Integer.parseInt(ending[0]),Integer.parseInt(ending[1]),firebaseUser.getUid());
                                routePresenterRT.add(route);


                            }
                        })
                .setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

        return builder.create();
    }
}