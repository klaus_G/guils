package com.estructurasdatos.guils.view.ui.driver_section.turn_driver;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.adapters.AdapterDriverFirestore;
import com.estructurasdatos.guils.interfaces.CRUDInterface;
import com.estructurasdatos.guils.model.Driver;
import com.estructurasdatos.guils.model.User;
import com.estructurasdatos.guils.presenter.DriverPresenterRT;
import com.estructurasdatos.guils.presenter.UserPresenter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class TurnDriverFragment extends Fragment {
    public static TurnDriverFragment newInstance() {
        return new TurnDriverFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_turndriver_section, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v=inflater.inflate(R.layout.fragment_turndriver_section,null);

        final TextView txt_turn_photo=v.findViewById(R.id.txt_turn_photo);
        final TextView txt_turn_licence=v.findViewById(R.id.txt_turn_licence);

        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        final UserPresenter userPresenter = new UserPresenter();
        final DriverPresenterRT driverPresenterRT = new DriverPresenterRT();

        Button bt = getActivity().findViewById(R.id.bt_turndriver);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userPresenter.element(firebaseUser.getUid(), new CRUDInterface.ElementListener<User>() {
                    @Override
                    public void onReaction(User user) {
                        Driver driver = new Driver(user,String.valueOf(txt_turn_licence.getText()));
                        driverPresenterRT.add(driver);
                    }
                });

            }
        });
    }
}
