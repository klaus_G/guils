package com.estructurasdatos.guils.view.ui.driver_section.route;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.adaptersRT.AdapterRouteFirebase;
import com.estructurasdatos.guils.data_Structure.DynamicArray;
import com.estructurasdatos.guils.model.Route;
import com.estructurasdatos.guils.util.Interface.RecyclerViewOnItemClickListener;
import com.estructurasdatos.guils.view.dialog.RoutesDeleteDialog;
import com.estructurasdatos.guils.view.dialog.RoutesDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.FirebaseFirestore;

public class RoutesDriverFragment extends Fragment {

    private RecyclerView recyclerView;
    private AdapterRouteFirebase adapter;
    private DynamicArray<Route> routeDynamicArray;

    public static RoutesDriverFragment newInstance() {
        return new RoutesDriverFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.routes_driver_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FloatingActionButton fab = getActivity().findViewById(R.id.fab_route_fragment);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new RoutesDialog().show(getFragmentManager(),"RoutesDialog");
            }
        });

        FloatingActionButton fab2 = getActivity().findViewById(R.id.fab_route_fragment_delete);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new RoutesDialog().show(getFragmentManager(),"RoutesDialog");
            }
        });

        routeDynamicArray=new DynamicArray<>();

        recyclerView = (RecyclerView)getActivity().findViewById(R.id.recycler_routes);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        adapter=new AdapterRouteFirebase(getContext(), routeDynamicArray, new RecyclerViewOnItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                new RoutesDeleteDialog().show(getFragmentManager(),"RouteDeleteDialog");
            }
        });

        recyclerView.setAdapter(adapter);

        FirebaseDatabase.getInstance().getReference().child("routes").addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                routeDynamicArray.removeAll();

                for (DataSnapshot snapshot:
                        dataSnapshot.getChildren()) {

                    final Route route=snapshot.getValue(Route.class);
                    routeDynamicArray.pushBack(route);

                }

                adapter.notifyDataSetChanged();

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }



}
