package com.estructurasdatos.guils.view.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.model.News;
import com.estructurasdatos.guils.presenter.NewsPresenterRT;
import com.estructurasdatos.guils.util.Generator;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Date;

public class NewsDialog extends DialogFragment {
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return createNewsDialog();
    }

    public AlertDialog createNewsDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.layout_create_news, null);

        builder.setView(v);

        Button btn_adj = v.findViewById(R.id.btn_adj);
        final EditText edtxt_contnews = v.findViewById(R.id.edtxt_contnews);
        final CheckBox checkBox = v.findViewById(R.id.cbx_priority);
        builder.setTitle("Publicar Noticia")
                .setPositiveButton("Publicar",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                final NewsPresenterRT newsPresenterRT = new NewsPresenterRT();
                                final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                                final int priority = (checkBox.isChecked()) ? 1 : 0;
                                final Date date = new Date();
                                Generator generator = new Generator();
                                News news = new News(firebaseUser.getUid(),
                                        firebaseUser.getDisplayName(),
                                        String.valueOf(edtxt_contnews.getText()),
                                        priority,
                                        date.toString(),
                                        generator.generateLatitude(),
                                        generator.generateLongitude());

                                newsPresenterRT.add(news);



                            }
                        })
                .setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

        return builder.create();
    }
}
