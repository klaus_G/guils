package com.estructurasdatos.guils.view.ui.main_section.test;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.data_Structure.HashTableMap;
import com.estructurasdatos.guils.model.CRUD.AllUserListener;
import com.estructurasdatos.guils.model.News;
import com.estructurasdatos.guils.model.User;
import com.estructurasdatos.guils.presenter.NewsPresenterRT;
import com.estructurasdatos.guils.presenter.UserPresenter;
import com.estructurasdatos.guils.util.Generator;
import com.estructurasdatos.guils.util.TimeCount;
import com.google.firebase.database.FirebaseDatabase;

public class TestFragment extends Fragment {

    private Button btn_reset_trips;
    private Button btn_reset_news;
    private Button btn_add_trips;
    private Button btn_add_news;
    private Button btn_search_trips;
    private Button btn_sort_news;
    private EditText txt_number;
    private EditText txt_search;
    private NewsPresenterRT newsPresenterRT;
    private UserPresenter userPresenter;
    public static TestFragment newInstance() {
        return new TestFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.test_fragment, container, false);

    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btn_reset_trips=getActivity().findViewById(R.id.btn_test_reset_trips);
        btn_reset_news=getActivity().findViewById(R.id.btn_test_reset_news);
        btn_add_trips=getActivity().findViewById(R.id.btn_test_add_trips);
        btn_add_news=getActivity().findViewById(R.id.btn_test_add_news);
        btn_search_trips=getActivity().findViewById(R.id.btn_search_trip);
        btn_sort_news=getActivity().findViewById(R.id.btn_sort_news);
        txt_number=getActivity().findViewById(R.id.number_elements);
        txt_search=getActivity().findViewById(R.id.txt_search_trip);
        newsPresenterRT=new NewsPresenterRT();
        userPresenter=new UserPresenter();


        btn_reset_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Generator generator=new Generator();
                Toast.makeText(getActivity(),"AGREGANDO "+String.valueOf(txt_number.getText()),Toast.LENGTH_SHORT).show();
                for (int i = 0; i<Integer.parseInt(String.valueOf(txt_number.getText())); i++){
                    User user= generator.generateUser();
                    userPresenter.add(user);

                }
                Toast.makeText(getActivity(),"AGREGADO",Toast.LENGTH_SHORT).show();

            }
        });

        btn_reset_trips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String s=" Tiempo: "+"1";
                Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();
                userPresenter.getUsers(new AllUserListener() {
                    @Override
                    public void onReaction(HashTableMap<String, User> users) {

                            TimeCount timeCount=new TimeCount();
                            timeCount.settInicio(System.currentTimeMillis());
                           // users.get("")
                            timeCount.settFinal(System.currentTimeMillis());


                        String s=" Tiempo: "+timeCount.geteEj();
                        Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();

                    }
                });

            }
        });

        btn_add_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Generator generator=new Generator();
                Toast.makeText(getActivity(),"AGREGANDO "+String.valueOf(txt_number.getText()),Toast.LENGTH_LONG).show();
                for (int i = 0; i<Integer.parseInt(String.valueOf(txt_number.getText())); i++){
                    News news=generator.generateNews();
                    newsPresenterRT.add(news);

                }
                Toast.makeText(getActivity(),"AGREGADO",Toast.LENGTH_SHORT).show();



            }
        });

        btn_sort_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseDatabase.getInstance().getReference("users").setValue(null);

            }
        });
    }

}
