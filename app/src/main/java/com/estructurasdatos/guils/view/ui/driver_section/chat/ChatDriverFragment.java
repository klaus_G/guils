package com.estructurasdatos.guils.view.ui.driver_section.chat;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.adapters.AdapterChatFirestore;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class ChatDriverFragment extends Fragment {
    private RecyclerView recyclerView;
    private CollectionReference chats;
    private AdapterChatFirestore adapter;

    public static ChatDriverFragment newInstance() {
        return new ChatDriverFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        chats=db.collection("chats");
        View rootView = inflater.inflate(R.layout.chat_driver_fragment, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_chats);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        adapter = new AdapterChatFirestore(this.getContext(), chats);

        recyclerView.setAdapter(adapter);


        recyclerView.setItemAnimator(new DefaultItemAnimator());

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

}
