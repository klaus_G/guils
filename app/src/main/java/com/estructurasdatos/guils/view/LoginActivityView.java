package com.estructurasdatos.guils.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.interfaces.CRUDInterface;
import com.estructurasdatos.guils.interfaces.LoginViewInterface;
import com.estructurasdatos.guils.model.User;
import com.estructurasdatos.guils.presenter.LoginPresenter;
import com.estructurasdatos.guils.presenter.UserPresenter;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

public class LoginActivityView extends AppCompatActivity implements LoginViewInterface, View.OnClickListener {

    SignInButton signInButton;
    GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN=123;
    private FirebaseAuth mAuth;
    private static final String TAG = "LoginActivityView";
    private UserPresenter userPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        signInButton = findViewById(R.id.sign_in_button);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this,gso);
        userPresenter=new UserPresenter(this,getString(R.string.default_web_client_id));
        signInButton.setOnClickListener(this);
        mAuth = FirebaseAuth.getInstance();

    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    @Override
    public void onClick(View v) {
        startActivityForResult(userPresenter.signIn(),RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {

                GoogleSignInAccount account = task.getResult(ApiException.class);
                //Se ingreso por Google de forma correcta, se realiza el "ingreso" a firebase
                firebaseAuthWithGoogle(account);

            } catch (ApiException e) {

                Log.w(TAG, "Google sign in failed", e);

            }
        }


    }


    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {

        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>(){
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Toast.makeText(getApplicationContext(),"Ingresando con "+user.getEmail(),Toast.LENGTH_SHORT).show();
                            verUnal(user);
                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(getApplicationContext(),"No se pudo ingresar a la cuenta de Google",Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                    }
                });
    }

    @Override
    public void updateUI(FirebaseUser firebaseUser) {
        LoginPresenter loginPresenter = new LoginPresenter();
        if(loginPresenter.updateUI(firebaseUser)){
            Intent intent = new Intent(getApplicationContext(),MainActivityView.class);

            startActivity(intent);
            finish();
        }
    }

    @Override
    public void userRegistration(FirebaseUser firebaseUser) {
        //Intent intent = new Intent(getApplicationContext(), RegistActivityView.class);
        //startActivity(intent);
    }

    @Override
    public void verUnal(FirebaseUser firebaseUser) {
        String dom=firebaseUser.getEmail().split("@")[1];
        if(!dom.equals("unal.edu.co")){
            userPresenter.erase(firebaseUser);
            Toast.makeText(getApplicationContext(),"Se debe ingresar con una cuenta de unal.edu.co",Toast.LENGTH_LONG).show();
        }else{
            userRegistration(firebaseUser);
            userRegistrationBD(firebaseUser);
            updateUI(firebaseUser);

        }
    }

    private void userRegistrationBD(FirebaseUser firebaseUser) {
        userPresenter.add(firebaseUser);
        userPresenter.element(firebaseUser.getUid(), new CRUDInterface.ElementListener<User>() {
            @Override
            public void onReaction(User user) {
                Toast.makeText(getApplicationContext(),user.getName(),Toast.LENGTH_LONG);
            }
        });
    }
}
