package com.estructurasdatos.guils.view.ui.main_section;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.estructurasdatos.guils.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainSectionFragment extends Fragment {


    View view;

    public static MainSectionFragment newInstance() {
        return new MainSectionFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.main_section_fragment, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        BottomNavigationView navView = view.findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_news, R.id.navigation_trips, R.id.navigation_chat,R.id.navigation_test)
                .build();
        NavController navController = Navigation.findNavController((AppCompatActivity)getActivity(), R.id.nav_host_fragment_bottom);
        NavigationUI.setupActionBarWithNavController((AppCompatActivity)getActivity(),navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

    }

}
