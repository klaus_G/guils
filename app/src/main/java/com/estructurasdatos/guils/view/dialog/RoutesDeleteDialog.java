package com.estructurasdatos.guils.view.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.fragment.app.DialogFragment;

import com.estructurasdatos.guils.R;

public class RoutesDeleteDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return createRoutesDialog();
    }

    public AlertDialog createRoutesDialog(){

        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v=inflater.inflate(R.layout.layout_delete_routes,null);

        builder.setView(v);

        builder.setTitle("Agregar Ruta");


        return builder.create();
    }

}
