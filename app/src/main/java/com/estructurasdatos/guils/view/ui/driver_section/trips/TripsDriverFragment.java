package com.estructurasdatos.guils.view.ui.driver_section.trips;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.adapters.AdapterTripFirestore;
import com.estructurasdatos.guils.view.dialog.TripsDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class TripsDriverFragment extends Fragment {

    private TripsDriverViewModel mViewModel;
    private RecyclerView recyclerView_accepted;
    private RecyclerView recyclerView_pending;
    private AdapterTripFirestore adapter_accepted;
    private AdapterTripFirestore adapter_pending;

    public static TripsDriverFragment newInstance() {
        return new TripsDriverFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.trips_driver_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FloatingActionButton fab = getActivity().findViewById(R.id.fab_tripdriver_fragment);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TripsDialog().show(getFragmentManager(),"TripsDialog");
            }
        });

        recyclerView_accepted=getActivity().findViewById(R.id.recycler_trip_accepted);
        recyclerView_pending=getActivity().findViewById(R.id.recycler_trip_pending);
        recyclerView_accepted.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView_pending.setLayoutManager(new LinearLayoutManager(getActivity()));

        /*FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference collection_accepted = db.collection("");
        CollectionReference collection_pending= db.collection("");
        adapter_accepted=new AdapterTripFirestore(getActivity(),collection_accepted);
        adapter_pending=new AdapterTripFirestore(getActivity(),collection_pending);

        recyclerView_accepted.setAdapter(adapter_accepted);
        recyclerView_pending.setAdapter(adapter_pending);*/

    }

}
