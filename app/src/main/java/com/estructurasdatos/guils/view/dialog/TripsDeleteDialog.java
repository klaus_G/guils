package com.estructurasdatos.guils.view.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.estructurasdatos.guils.R;

public class TripsDeleteDialog extends DialogFragment {
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return createTripDialog();
    }

    public AlertDialog createTripDialog(){
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v=inflater.inflate(R.layout.layout_delete_trips,null);

        builder.setView(v);

        builder.setTitle("Agregar Viaje");

        //recycler click

        return builder.create();

    }
}
