package com.estructurasdatos.guils.view.ui.main_section.trips;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.adapters.AdapterTripFirestore;

public class TripsFragment extends Fragment {

    private TripsViewModel mViewModel;

    private RecyclerView recyclerView_clickeable;
    private RecyclerView recyclerView;
    private AdapterTripFirestore adapter_clickeable;
    private AdapterTripFirestore adapter;



    public static TripsFragment newInstance() {
        return new TripsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.trips_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        recyclerView = (RecyclerView)getActivity().findViewById(R.id.title_text_recycler_trips);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        


    }

}
