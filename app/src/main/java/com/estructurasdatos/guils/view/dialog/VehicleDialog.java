package com.estructurasdatos.guils.view.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.model.Vehicle;
import com.estructurasdatos.guils.presenter.VehiclePresenter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class VehicleDialog extends DialogFragment {
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return createVehicleDialog();
    }

    public AlertDialog createVehicleDialog(){
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View v=inflater.inflate(R.layout.layout_create_vehicle,null);

        builder.setView(v);

        final EditText ed_type=v.findViewById(R.id.ed_car_type);
        final EditText ed_model=v.findViewById(R.id.ed_car_model);
        final EditText ed_brand=v.findViewById(R.id.ed_car_brand);
        final EditText ed_color=v.findViewById(R.id.ed_car_color);
        final EditText ed_plate=v.findViewById(R.id.ed_car_plate);

        builder.setTitle("Agregar Vehículo")
                .setPositiveButton("Agregar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final VehiclePresenter vehiclePresenterRT= new VehiclePresenter();
                        final FirebaseUser firebaseUser= FirebaseAuth.getInstance().getCurrentUser();

                        Vehicle vehicle = new Vehicle(String.valueOf(ed_plate.getText()), String.valueOf(ed_brand.getText()), String.valueOf(ed_model.getText()), String.valueOf(ed_color.getText()),String.valueOf(ed_type.getText()));
                        vehiclePresenterRT.add(firebaseUser.getUid(),vehicle);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        return builder.create();

    }
}
