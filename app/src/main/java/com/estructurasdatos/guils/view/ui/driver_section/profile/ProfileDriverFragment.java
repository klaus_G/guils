package com.estructurasdatos.guils.view.ui.driver_section.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.adaptersRT.AdapterVehicleFirebase;
import com.estructurasdatos.guils.data_Structure.DynamicArray;
import com.estructurasdatos.guils.model.Vehicle;
import com.estructurasdatos.guils.util.Interface.RecyclerViewOnItemClickListener;
import com.estructurasdatos.guils.view.dialog.VehicleDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ProfileDriverFragment extends Fragment {

    private RecyclerView recyclerView;
    private AdapterVehicleFirebase adapter;
    private DynamicArray<Vehicle> vehicleDynamicArray;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile_driver_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FloatingActionButton fab = getActivity().findViewById(R.id.fab_add_vehicle);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new VehicleDialog().show(getFragmentManager(),"VehicleDialog");
            }
        });

        vehicleDynamicArray=new DynamicArray<>();

        recyclerView = (RecyclerView)getActivity().findViewById(R.id.recycler_vehicle);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        adapter = new AdapterVehicleFirebase(getContext(), vehicleDynamicArray, new RecyclerViewOnItemClickListener() {
            @Override
            public void onClick(View v, int position) {

            }
        });

        recyclerView.setAdapter(adapter);

        FirebaseDatabase.getInstance().getReference().child("vehiclesList").addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                vehicleDynamicArray.removeAll();

                for (DataSnapshot snapshot:
                        dataSnapshot.getChildren()) {

                    final Vehicle vehicle=snapshot.getValue(Vehicle.class);
                    vehicleDynamicArray.pushBack(vehicle);

                }

                adapter.notifyDataSetChanged();

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }
}
