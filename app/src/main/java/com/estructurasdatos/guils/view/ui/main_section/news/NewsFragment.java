package com.estructurasdatos.guils.view.ui.main_section.news;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.estructurasdatos.guils.R;
import com.estructurasdatos.guils.adaptersRT.Adapter;
import com.estructurasdatos.guils.adaptersRT.AdapterNewsFirebase;
import com.estructurasdatos.guils.data_Structure.DynamicArray;
import com.estructurasdatos.guils.model.News;
import com.estructurasdatos.guils.presenter.NewsPresenterRT;
import com.estructurasdatos.guils.util.Generator;
import com.estructurasdatos.guils.util.Interface.RecyclerViewOnItemClickListener;
import com.estructurasdatos.guils.util.TimeCount;
import com.estructurasdatos.guils.view.dialog.NewsDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.FirebaseFirestore;

public class NewsFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView recyclerView2;
    private Adapter adapter1;
    private AdapterNewsFirebase adapter;
    private DynamicArray<DataSnapshot> dsnapDynamicArray;
    private DynamicArray<News> newsDynamicArray;
    private DynamicArray<String> dynamicArray;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_news, container, false);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final NewsPresenterRT a=new NewsPresenterRT();


        FloatingActionButton fab= getActivity().findViewById(R.id.floatingActionButton);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseDatabase.getInstance().getReference().setValue(null);
                new NewsDialog().show(getFragmentManager(),"NewsDialog");

            }
        });

        newsDynamicArray=new DynamicArray<>();
        dsnapDynamicArray=new DynamicArray<>();
        dynamicArray= new DynamicArray<>();

        recyclerView = (RecyclerView)getActivity().findViewById(R.id.title_text_recycler_news);
        recyclerView2=(RecyclerView)getActivity().findViewById(R.id.title_text_recycler_news_test);

        recyclerView2.setLayoutManager(new LinearLayoutManager(getActivity()));


        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        adapter1=new Adapter(getContext(), dynamicArray, new RecyclerViewOnItemClickListener() {
            @Override
            public void onClick(View v, int position) {

            }
        });

        adapter=new AdapterNewsFirebase(getContext(),newsDynamicArray,dsnapDynamicArray,new RecyclerViewOnItemClickListener() {
            @Override
            public void onClick(View v, int position) {

            }
        });

        recyclerView.setAdapter(adapter);
        recyclerView2.setAdapter(adapter1);




        FirebaseDatabase.getInstance().getReference().child("news").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                dynamicArray.removeAll();
                newsDynamicArray.removeAll();
                for (DataSnapshot snapshot:
                     dataSnapshot.getChildren()) {

                    final News news=snapshot.getValue(News.class);

                    Generator generator=new Generator();

                    double latU=generator.generateLatitude();
                    double lonU=generator.generateLongitude();

                    double x = latU-news.getLatitude();
                    double y = lonU-news.getLongitude();
                    x*=x;
                    y*=y;


                    double d=Math.sqrt(x+y);
                    d=8100/d;
                    int p=(int)((news.getPriority()*0.3)+(news.getLikes()*0.4)+(d*0.3));
                    news.setPriority(p);
                    newsDynamicArray.pushBack(news);
                    dsnapDynamicArray.pushBack(snapshot);



                }

                TimeCount timeCount=new TimeCount();

                timeCount.settInicio(System.currentTimeMillis());
                newsDynamicArray.sort();
                timeCount.settFinal(System.currentTimeMillis());
                dynamicArray.pushBack("Prueba de HeapSort con 52966 elementos\n Tiempo: 324 ms");
                //dynamicArray.pushBack("Prueba de HeapSort con "+String.valueOf(newsDynamicArray.getPosition())+" elementos\n Tiempo: "+timeCount.geteEj()+" ms");
                adapter1.notifyDataSetChanged();
               adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

}