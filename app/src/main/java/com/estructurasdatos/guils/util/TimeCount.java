package com.estructurasdatos.guils.util;

public class TimeCount {
    private long tInicio;
    private long tFinal;
    private long tEj;

    public TimeCount() {
        this.tInicio=0;
        this.tFinal=0;
        this.tEj=0;
    }

    public long gettInicio() {
        return tInicio;
    }

    public void settInicio(long tInicio) {
        this.tInicio = tInicio;
    }

    public long gettFinal() {
        return tFinal;
    }

    public void settFinal(long tFinal) {
        this.tFinal = tFinal;
    }

    public long gettEj() {
        return tFinal-tInicio;
    }

    public String geteEj(){
        return String.valueOf(gettEj());
    }

    public void settEj(long tEj) {
        this.tEj = tEj;
    }
}
