/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.estructurasdatos.guils.util;

import com.estructurasdatos.guils.data_Structure.DynamicArray;
import com.estructurasdatos.guils.model.Driver;
import com.estructurasdatos.guils.model.News;
import com.estructurasdatos.guils.model.Route;
import com.estructurasdatos.guils.model.Trip;
import com.estructurasdatos.guils.model.User;
import com.estructurasdatos.guils.model.Vehicle;

import java.text.SimpleDateFormat;
import java.util.concurrent.ThreadLocalRandom;

public class Generator {
    
    private DynamicArray<Driver> availableDrivers = new DynamicArray<Driver>();
    private DynamicArray<Route> availableRoutes = new DynamicArray<Route>();
    private DynamicArray<User> users = new DynamicArray<User>();
    private DynamicArray<News> publishedNews = new DynamicArray<News>();
    private DynamicArray<Vehicle> vehicles = new DynamicArray<Vehicle>();
    private DynamicArray<Trip> trips = new DynamicArray<Trip>();
    
    

    public Generator() {

    }
    
    private String generarCadenaSinSimbolosLongitudRandom(int minLength, int maxLength){
        StringBuilder s = new StringBuilder();
        int randomLength = ThreadLocalRandom.current().nextInt(minLength, maxLength+1); //Generar un entero
        for(int i=0; i<randomLength; i++){            
            int randomLetter = ThreadLocalRandom.current().nextInt(65, 91); //Generar un entero en [33,91] from " !  " to " _ ", 
            //therefore the password will contain symbols and numbers
            s.append(Character.toString((char) randomLetter)); //Convertir el entero a su representación ASCII y concatenar
        }
        return new String(s);
    }

    private String generarCadenaConSimbolosLongitudRandom(int minLength, int maxLength, int minAscii, int maxAscii){
        StringBuilder s = new StringBuilder();
        int randomLength = ThreadLocalRandom.current().nextInt(minLength, maxLength+1); //Generar un entero
        for(int i=0; i<randomLength; i++){            
            int randomLetter = ThreadLocalRandom.current().nextInt(minAscii, maxAscii+1); //Generar un entero en [33,91] from " !  " to " _ ", 
            //therefore the password will contain symbols and numbers
            s.append(Character.toString((char) randomLetter)); //Convertir el entero a su representación ASCII y concatenar
        }
        
        return new String(s);
    }

    public String generarCadenaSinSimbolosLongitudFija(int length){
        StringBuilder s = new StringBuilder();
        for(int i=0; i<length; i++){            
            int randomLetter = ThreadLocalRandom.current().nextInt(65, 91); //Generar un entero en [33,91] from " !  " to " _ ", 
            //therefore the password will contain symbols and numbers
            s.append(Character.toString((char) randomLetter)); //Convertir el entero a su representación ASCII y concatenar
        }
        return new String(s);
    }
    
    private String generarCadenaConSimbolosLongitudFija(int minLength, int maxLength, int minAscii, int maxAscii, int length){
        StringBuilder s = new StringBuilder();
        for(int i=0; i<length; i++){            
            int randomLetter = ThreadLocalRandom.current().nextInt(minAscii, maxAscii+1); //Generar un entero en [33,91] from " !  " to " _ ", 
            //therefore the password will contain symbols and numbers
            s.append(Character.toString((char) randomLetter)); //Convertir el entero a su representación ASCII y concatenar
        }
        
        return new String(s);
    }

    public int randomNum(int min, int max){
        return ThreadLocalRandom.current().nextInt(min, max+1);
    }
    
     public double randomDouble(int min, int max){
        return ThreadLocalRandom.current().nextInt(min, max+1);
    }
    
    //Para usuarios
    private String generateFullName(){
        String[] nombres ={ "Cristian", "Daniela", "Laura", "Paula", "Monica", "Jose", "Ivan", "David", "Sebastian", "Sofia", "Juana", "Daniel", "Rodrigo", "Santiago" , "Lorena"};
        String[] apellidos ={ "Tovar", "Bejarano", "Solano", "Martinez", "Cornejo", "Cardenas", "Vallejo", "Ruiz", "Espinoza", "Valencia", "Santana"};
        StringBuilder s = new StringBuilder();
        int indexNom = ThreadLocalRandom.current().nextInt(0, nombres.length);
        int indexApe = ThreadLocalRandom.current().nextInt(0, apellidos.length);
        
        s.append(nombres[indexNom]);
        s.append(" ");
        s.append(apellidos[indexApe]);
        return new String(s);
    }
    
    private String generateEmail(){
        
        //Determinar longitud pseudoaleatoria del email
        int randomLength = ThreadLocalRandom.current().nextInt(8, 20); //Generar un entero [8,20)
        StringBuilder s = new StringBuilder();
        for(int i=0; i<randomLength; i++){            
            int randomLetter = ThreadLocalRandom.current().nextInt(65, 90 + 1); //Generar un entero en [65,91)
            s.append(Character.toString((char) randomLetter)); //Convertir el entero a su representación ASCII y concatenar
        }
        s.append("@");
        String[] dominios = {"hotmail.com", "hotmail.es", "gmail.com", "outlook.com", "outlook.es", "yahoo.es"};
        int indexDominio = ThreadLocalRandom.current().nextInt(0, dominios.length);
        s.append(dominios[indexDominio]);
        return new String(s);
    }
    
    private int generatePhone(){
        int prefix = ThreadLocalRandom.current().nextInt(300, 323);
        int suffix = ThreadLocalRandom.current().nextInt(0000000, 9999999);
        int phone = (prefix * 1000000) + suffix;
        return phone;
    }
    
    private String generatePhoto(){
        return generarCadenaConSimbolosLongitudRandom(10, 12, 47, 123);
    }
    
    private int generateRating(){
        return ThreadLocalRandom.current().nextInt(0, 11);
    };
    
    public User generateUser(){
        User user = new User( generateFullName(), generateEmail(), String.valueOf(generatePhone()), generatePhoto(), generateRating() );
        users.pushBack(user);
        return user;
    }

    //For drivers...
    public Driver generateDriver(int numVehicles){ //cuantos vechiculos?
        DynamicArray<Vehicle> vehicles = new DynamicArray<Vehicle>();
        int howMany = randomNum(1, numVehicles+1);
        for (int i = 0; i < howMany; i++) {
            vehicles.pushBack( generateVehicle() );
        }        
        Driver driver = new Driver(generateUser(),generarCadenaSinSimbolosLongitudFija(6));
        availableDrivers.pushBack(driver);
        return driver;
    }
    
    //For vehicles
    private String generatePlate(){
        String plate = "";
        for(int i=0; i<3; i++){
            int randomLetter = ThreadLocalRandom.current().nextInt(65, 90 + 1); //Generar un entero en [65,91]
            plate += Character.toString((char) randomLetter); //Convertir el entero a su representación ASCII y concatenar
        }        
        int randomNum = ThreadLocalRandom.current().nextInt(0, 1000); //Parte numérica de la placa
        plate += randomNum;
        return plate;
    }
    
     private String generateColor(){
        String[] colors ={ "Rojo", "Azul", "Amarillo", "Verde", "Gris", "Negro", "Purpura", "Naranja", "Marrón"};
        String color = "";
        int index = ThreadLocalRandom.current().nextInt(0, colors.length);
        color += colors[index];
        return color;
    }
    
    private String generateBrand(){
        String[] brands ={ "Hyundai", "Mazda", "Subaru", "Renault", "Chevrolet" , "Lamborghini", "Bugatti", "Ferrari" };
        String brand = "";
        int index = ThreadLocalRandom.current().nextInt(0, brands.length);
        brand += brands[index];
        return brand;
    }
    
    private String generateModel(){
        String[] models ={ "1995", "1998", "2002", "2003", "2009" , "2013", "2015", "2018", "2020" };
        String model = "";
        int index = ThreadLocalRandom.current().nextInt(0, models.length);
        model += models[index];
        return model;
    }
    
    private String generateType(){
        String[] types ={ "Car", "Van", "Taxi", "Bus"};
        String type = "";
        int index = ThreadLocalRandom.current().nextInt(0, types.length);
        type += types[index];
        return type;
    }
    
    public Vehicle generateVehicle(){
        Vehicle vehicle = new Vehicle( generatePlate(), generateBrand(), generateModel(), generateColor(), generateType() );
        vehicles.pushBack(vehicle);
        return vehicle;
    }
    
    //For news...
    private int generateUserId(){
        return randomNum(0, 999999);
    }
    
    private String generateUserString(){
        return generarCadenaSinSimbolosLongitudRandom(10, 15);
    }
    
    private String generateText(){
        return generarCadenaConSimbolosLongitudRandom(10, 400, 32, 126);
    }
    
    private int generatePriority(){
        return randomNum(0, 100);
    }

    private String generateDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        StringBuilder d = new StringBuilder();
        d.append(randomNum(1, 31)).append("-");
        d.append(randomNum(1, 13)).append("-");
        d.append(randomNum(2019, 2021)).append(" ");
        d.append(randomNum(0, 23)).append(":"); //hora
        d.append(randomNum(0, 60)).append(":"); //minuto
        d.append(randomNum(0, 60)); //seg
        
	String dateInString = new String(d);

        return dateInString;
    }
    


    public double generateLatitude(){
        return randomNum(-90, 90);
    }

    public double generateLongitude(){
        return randomNum(-180, 180);
    }
    
    public News generateNews(){        
        News news = new News( String.valueOf(generateUserId()), generateUserString(), generateText(), generatePriority(), generateDate(),generateLatitude(),generateLongitude());
        publishedNews.pushBack(news);
        return news;
    }
    /*
    
    //For Routes...
    private String generatePath(){
        generarCadenaConSimbolosLongitudRandom(13, 20, 33, 126);
    }
    
    public Route generateRoute(){
        //Pick a driver from the available ones
        int index = randomNum(0, availableDrivers.size()-1);
        Driver driver = availableDrivers.get(index);
        Route route = new Route( generatePath(), randomDouble(-90, 90) , randomDouble(-180, 180) , randomDouble(-90, 90), randomDouble(-180, 180), driver  );
        availableRoutes.add(route);
        return route;
    }
    
    //For Trips...
    public Trip generateTrip(){
        //Pick a route...
        int index = randomNum(0, availableRoutes.size()-1);
        Route route = availableRoutes.g(index);
        boolean[] days = new boolean[7];
        for (int i = 0; i < 7; i++) {
            if( randomNum(0, 1) = 0 ){
                days[i] = false;
            } else {
                days[i] = true;
            };
            
        }
         Date date = new Date(); 
        long millis = date.getTime();
        // https://docs.oracle.com/javase/7/docs/api/java/sql/Time.html
        Trip trip = new Trip( route, days, new Time(millis), randomNum(0, 12), randomNum(0, 100)  );
        trips.pushBack(trip);
        return trip;
    }
    */
    
    
    
    
   
}
