package com.estructurasdatos.guils.model.CRUD_RT;

import android.util.Log;

import androidx.annotation.NonNull;

import com.estructurasdatos.guils.interfaces.CRUDInterfaceRT;
import com.estructurasdatos.guils.model.Chat;
import com.estructurasdatos.guils.model.Message;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MessagesFirebase implements CRUDInterfaceRT<Message> {
    private FirebaseDatabase db;
    private DatabaseReference node;

    public MessagesFirebase(){
        db = FirebaseDatabase.getInstance();
        node = db.getReference().child("Messages");
    }

    @Override
    public void element(String id, final ElementListener<Message> listener) {
        final String id_ = id;
        node.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                final Message message = dataSnapshot.getValue(Message.class);
                listener.onReaction(message);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("Firebase","Error_Read", databaseError.toException());
                listener.onReaction(null);
            }
        });
    }

    @Override
    public void add(Message message) {
        node.push().setValue(message);
    }

    public void add(String id, Message message){
        node.child(id).setValue(message);
    }

    @Override
    public String newElement() {
        return node.push().getKey();
    }

    @Override
    public void erase(String id) {
        node.child(id).setValue(null);
    }

    @Override
    public void update(String id, Message message) {
        node.child(id).setValue(message);
    }

    @Override
    public void size(final SizeListener listener) {
        node.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listener.onReaction(dataSnapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("Firebase","Error_size", databaseError.toException());
                listener.onReaction(-1);
            }
        });
    }
}
