package com.estructurasdatos.guils.model.CRUD;

import android.util.Log;

import androidx.annotation.NonNull;

import com.estructurasdatos.guils.interfaces.CRUDInterface;
import com.estructurasdatos.guils.model.News;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

public class NewsFirestore implements CRUDInterface<News> {

    private CollectionReference news;

    public NewsFirestore() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        news=db.collection("news");
    }

    @Override
    public void element(String id, final ElementListener<News> listener) {
        news.document(id).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    News news_ = task.getResult().toObject(News.class);
                    listener.onReaction(news_);
                }else{
                    Log.e("Firebase","Error al leer",task.getException());
                    listener.onReaction(null);
                }
            }
        });
    }

    @Override
    public void add(News news) {
        this.news.document().set(news);
    }

    public void add(News news,String id) {
        this.news.document(id).set(news);
    }

    @Override
    public void newElement() {
        news.document().getId();
    }

    @Override
    public void erase(String id) {
        news.document(id).delete();
    }

    public void erase(String idNews,String idUser){
        this.news.document(idNews).collection("LikesList").document(idUser).delete();
    }

    @Override
    public void update(String id, News news) {
        this.news.document(id).set(news);
    }

    @Override
    public void size(final SizeListener listener) {
        news.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    listener.onReaction(task.getResult().size());
                }else {
                    Log.e("Firebase","Error en tamano",task.getException());
                    listener.onReaction(-1);
                }
            }
        });

    }
}
