package com.estructurasdatos.guils.model.CRUD_RT;

import android.util.Log;

import androidx.annotation.NonNull;

import com.estructurasdatos.guils.interfaces.CRUDInterfaceRT;
import com.estructurasdatos.guils.model.Trip;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class TripsFirebase implements CRUDInterfaceRT<Trip> {
    private FirebaseDatabase db;
    private DatabaseReference node;

    public TripsFirebase(){
        db = FirebaseDatabase.getInstance();
        node = db.getReference().child("trips");
    }

    @Override
    public void element(String id, final ElementListener<Trip> listener) {
        final String id_=id;
        node.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final Trip trip = dataSnapshot.getValue(Trip.class);
                listener.onReaction(trip);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("Firebase","Error_Read", databaseError.toException());
                listener.onReaction(null);
            }
        });
    }

    @Override
    public void add(Trip trip) {
        node.push().setValue(trip);
    }

    public void add(String id, Trip trip){
        node.push().child(id).setValue(trip);
    }

    @Override
    public String newElement() {
        return node.push().getKey();
    }

    @Override
    public void erase(String id) {
        node.child(id).setValue(null);
    }

    @Override
    public void update(String id, Trip trip) {
        node.child(id).setValue(trip);
    }

    @Override
    public void size(final SizeListener listener) {
        node.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listener.onReaction(dataSnapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("Firebase","Error_size", databaseError.toException());
                listener.onReaction(-1);
            }
        });
    }
}
