package com.estructurasdatos.guils.model.CRUD;

import android.util.Log;

import androidx.annotation.NonNull;

import com.estructurasdatos.guils.interfaces.CRUDInterface;
import com.estructurasdatos.guils.model.Route;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

public class RoutesFirestore implements CRUDInterface<Route> {

    private CollectionReference routes;

    public RoutesFirestore() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        routes=db.collection("routes");
    }

    @Override
    public void element(String id, final ElementListener<Route> listener) {
        routes.document(id).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    Route route = task.getResult().toObject(Route.class);
                    listener.onReaction(route);
                }else{
                    Log.e("Firebase","Error al leer",task.getException());
                    listener.onReaction(null);
                }
            }
        });
    }

    @Override
    public void add(Route route) {
        routes.document().set(route);
    }

    @Override
    public void newElement() {
        routes.document().getId();
    }

    @Override
    public void erase(String id) {
        routes.document(id).delete();
    }

    @Override
    public void update(String id, Route route) {
        routes.document(id).set(route);
    }

    @Override
    public void size(final SizeListener listener) {
        routes.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    listener.onReaction(task.getResult().size());
                }else {
                    Log.e("Firebase","Error en tamano",task.getException());
                    listener.onReaction(-1);
                }
            }
        });
    }
}
