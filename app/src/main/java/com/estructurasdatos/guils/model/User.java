package com.estructurasdatos.guils.model;
public class User{

    private String name;
    private String email;
    private String phone;
    private String photo;
    private double rating;

    public User(String name, String email, String phone, String photo, double rating) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.photo = photo;
        this.rating = rating;
    }

    public User(){

    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
