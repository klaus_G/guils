package com.estructurasdatos.guils.model;

import android.graphics.Bitmap;

import com.estructurasdatos.guils.data_Structure.DynamicArray;

import java.sql.Date;
import java.sql.Time;

public class Message {
    private User user;
    private Date date;
    private String message;
    private DynamicArray<String> pictureList;

    public Message(User user, Date date, String message, DynamicArray<String> pictureList) {
        this.user = user;
        this.date = date;
        this.message = message;
        this.pictureList = pictureList;
    }

    public Message() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DynamicArray<String> getPictureList() {
        return pictureList;
    }

    public void setPictureList(DynamicArray<String> pictureList) {
        this.pictureList = pictureList;
    }

    @Override
    public String toString() {
        return "Message{" +
                "user=" + user +
                ", date=" + date +
                ", message='" + message + '\'' +
                ", pictureList=" + pictureList +
                '}';
    }
}
