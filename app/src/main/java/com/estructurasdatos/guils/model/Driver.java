package com.estructurasdatos.guils.model;

import com.estructurasdatos.guils.data_Structure.HashTableMap;
import com.estructurasdatos.guils.data_Structure.interfaces.Map;
import com.google.firebase.database.Exclude;

import java.security.InvalidKeyException;

public class Driver extends User {
    private String DriversLicence;
    @Exclude
    private Map<String,Vehicle> vehiclesList;


    public Driver(String name, String email, String phone, String photo, int rating, String driversLicence, Map<String,Vehicle> vehiclesList) {
        super(name, email, phone, photo, rating);
        DriversLicence = driversLicence;
        this.vehiclesList = new HashTableMap<>();
    }

    public Driver(User user, String driversLicence) {
        super(user.getName(), user.getEmail(), user.getPhone(), user.getPhoto(), user.getRating());
        DriversLicence = driversLicence;
        this.vehiclesList = new HashTableMap<>();
    }

    public Driver(){
        this.vehiclesList=null;
    }

    public String getDriversLicence() {
        return DriversLicence;
    }

    public void setDriversLicence(String driversLicence) {
        DriversLicence = driversLicence;
    }

    @Exclude
    public Map<String,Vehicle> getVehiclesList() {
        return vehiclesList;
    }

    @Exclude
    public void setVehiclesList(Map<String,Vehicle> vehiclesList) {
        this.vehiclesList = vehiclesList;
    }
    @Exclude
    public void setElementVehiclesList(String key,Vehicle vehicle) {
        try {
            this.vehiclesList.put(key,vehicle);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
    }

}
