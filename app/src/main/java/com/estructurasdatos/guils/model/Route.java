package com.estructurasdatos.guils.model;

public class Route {
    private String path;
    private double lat_st;
    private double long_st;
    private double lat_fin;
    private double long_fin;
    private String driverId;

    public Route(String path, double lat_st, double long_st, double lat_fin, double long_fin, String driverId) {
        this.path = path;
        this.lat_st = lat_st;
        this.long_st = long_st;
        this.lat_fin = lat_fin;
        this.long_fin = long_fin;
        this.driverId = driverId;
    }

    public Route() {
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public double getLat_st() {
        return lat_st;
    }

    public void setLat_st(double lat_st) {
        this.lat_st = lat_st;
    }

    public double getLong_st() {
        return long_st;
    }

    public void setLong_st(double long_st) {
        this.long_st = long_st;
    }

    public double getLat_fin() {
        return lat_fin;
    }

    public void setLat_fin(double lat_fin) {
        this.lat_fin = lat_fin;
    }

    public double getLong_fin() {
        return long_fin;
    }

    public void setLong_fin(double long_fin) {
        this.long_fin = long_fin;
    }

    public String getDriver() {
        return driverId;
    }

    public void setDriver(String driver) {
        this.driverId = driverId;
    }

}
