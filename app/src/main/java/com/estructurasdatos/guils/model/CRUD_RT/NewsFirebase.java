package com.estructurasdatos.guils.model.CRUD_RT;

import android.util.Log;

import androidx.annotation.NonNull;

import com.estructurasdatos.guils.interfaces.CRUDInterfaceRT;
import com.estructurasdatos.guils.model.News;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class NewsFirebase implements CRUDInterfaceRT<News> {

    FirebaseDatabase firebaseDatabase;
    DatabaseReference node;

    public NewsFirebase() {
        this.firebaseDatabase = FirebaseDatabase.getInstance();
        this.node=firebaseDatabase.getReference().child("news");
    }

    @Override
    public void element(String id, ElementListener<News> listener) {



    }

    public void checkLike(String userId, String newsId, final ElementListener<Integer> listener){
        node.child(newsId).child("likesmap").child(newsId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                Integer isLiked=dataSnapshot.getValue(Integer.class);
                listener.onReaction(isLiked);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("Firebase","Error_Read", databaseError.toException());
                listener.onReaction(null);
            }
        });
    }

    @Override
    public void add(News news) {
        node.push().setValue(news);
    }

    public void add(String userId,String newsId,News news) {
        node.child(newsId).setValue(news);

        Iterable iterator=news.getLikesmap().values();
        for(Object o:iterator){
            Integer integer=(Integer) o;
            addLike(userId,newsId);
        }
    }


    public void addLike(String userId,String newsId) {

        node.child(newsId).child("likesmap").child(userId).setValue(1);

    }

    public void removeLike(String userId,String newsId) {

        node.child(newsId).child("likesmap").child(userId).setValue(null);

    }

    @Override
    public String newElement() {
        return null;
    }

    @Override
    public void erase(String id) {

    }

    @Override
    public void update(String id, News news) {
        node.child(id).setValue(news);

    }

    public void update(String userId,String newsId, News news) {
        node.child(newsId).setValue(news);
        Iterable iterator=news.getLikesmap().values();
        for(Object o:iterator){
            Integer integer=(Integer) o;
            addLike(userId,newsId);
        }

    }

    @Override
    public void size(SizeListener listener) {

    }
}
