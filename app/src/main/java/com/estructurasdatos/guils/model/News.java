package com.estructurasdatos.guils.model;

import com.estructurasdatos.guils.data_Structure.HashTableMap;
import com.google.firebase.database.Exclude;

import java.security.InvalidKeyException;

public class News{
    private String userId;
    private String user;
    private String text;
    @Exclude
    private HashTableMap<String,String> pictureList;
    private int priority;
    private int likes;
    @Exclude
    private HashTableMap<String,Integer> likesmap;
    private String date;
    private double latitude;
    private double longitude;

    public News(String userId, String user, String text, int priority, String date, double latitude, double longitude) {
        this.userId = userId;
        this.user = user;
        this.text = text;
        this.priority = priority;
        this.date = date;
        this.latitude = latitude;
        this.longitude = longitude;
        this.pictureList=new HashTableMap<>();
        this.likes=0;
        this.likesmap=new HashTableMap<>();
    }

    public News() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Exclude
    public HashTableMap<String, String> getPictureList() {
        return pictureList;
    }

    @Exclude
    public void setPictureList(HashTableMap<String, String> pictureList) {
        this.pictureList = pictureList;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public void addLike(){
        this.likes++;
    }

    public void removeLike(){
        this.likes--;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    @Exclude
    public HashTableMap<String, Integer> getLikesmap() {
        return likesmap;
    }

    @Exclude
    public void setLikesmap(HashTableMap<String, Integer> likesmap) {
        this.likesmap = likesmap;
    }

    @Exclude
    public void setElementLikesmap(String key,Integer integer) {
        if(likesmap==null) this.likesmap=new HashTableMap<>();
        try {
            this.likesmap.put(key,integer);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
