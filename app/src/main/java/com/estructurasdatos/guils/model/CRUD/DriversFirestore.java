package com.estructurasdatos.guils.model.CRUD;

import android.util.Log;

import androidx.annotation.NonNull;

import com.estructurasdatos.guils.interfaces.CRUDInterface;
import com.estructurasdatos.guils.model.Driver;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

public class DriversFirestore implements CRUDInterface<Driver> {

    private CollectionReference drivers;

    public DriversFirestore() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        drivers=db.collection("drivers");
    }

    @Override
    public void element(String id, final ElementListener<Driver> listener) {
        drivers.document(id).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    try {
                        Driver driver = task.getResult().toObject(Driver.class);
                        listener.onReaction(driver);
                    }catch (Exception ex){
                        Log.e("No_Driver",ex.toString());
                        listener.onReaction(null);
                    }

                }else{
                    Log.e("Firebase","Error al leer",task.getException());
                    listener.onReaction(null);
                }
            }
        });
    }

    @Override
    public void add(Driver driver) {
        drivers.document().set(driver);
    }

    @Override
    public void newElement() {
        drivers.document().getId();
    }

    @Override
    public void erase(String id) {
        drivers.document(id).delete();
    }

    @Override
    public void update(String id, Driver driver) {
        drivers.document(id).set(driver);
    }

    @Override
    public void size(final SizeListener listener) {
        drivers.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    listener.onReaction(task.getResult().size());
                }else {
                    Log.e("Firebase","Error en tamano",task.getException());
                    listener.onReaction(-1);
                }
            }
        });
    }
}
