package com.estructurasdatos.guils.model.CRUD;

import android.util.Log;

import androidx.annotation.NonNull;

import com.estructurasdatos.guils.interfaces.CRUDInterface;
import com.estructurasdatos.guils.model.Chat;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Collections;

public class ChatsFirestore implements CRUDInterface<Chat> {

    private CollectionReference chats;

    public ChatsFirestore() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        chats=db.collection("chats");
    }

    @Override
    public void element(String id, final ElementListener<Chat> listener) {
        chats.document(id).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    Chat chat = task.getResult().toObject(Chat.class);
                    listener.onReaction(chat);
                }else{
                    Log.e("Firebase","Error al leer",task.getException());
                    listener.onReaction(null);
                }
            }
        });
    }

    @Override
    public void add(Chat chat) {
        chats.document().set(chat);
    }

    @Override
    public void newElement() {
        chats.document().getId();
    }

    @Override
    public void erase(String id) {
        chats.document(id).delete();
    }

    @Override
    public void update(String id, Chat chat) {
        chats.document(id).set(chat);
    }

    @Override
    public void size(final SizeListener listener) {
        chats.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    listener.onReaction(task.getResult().size());
                }else {
                    Log.e("Firebase","Error en tamano",task.getException());
                    listener.onReaction(-1);
                }
            }
        });
    }
}
