package com.estructurasdatos.guils.model.CRUD_RT;

import android.util.Log;

import androidx.annotation.NonNull;

import com.estructurasdatos.guils.interfaces.CRUDInterfaceRT;
import com.estructurasdatos.guils.model.Vehicle;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

interface VehicleListener{
    void onReaction(DataSnapshot vehicles);
}

public class VehicleFirebase implements CRUDInterfaceRT<Vehicle> {



    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference node;
    public VehicleFirebase() {
        firebaseDatabase=FirebaseDatabase.getInstance();
        node=firebaseDatabase.getReference().child("vehicles");
    }

    @Override
    public void element(String id, ElementListener<Vehicle> listener) {

    }

    public void elementFromDriver(String id, final VehicleListener listener) {
        firebaseDatabase.getReference().child("drivers").child(id).child("vehiclesList").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listener.onReaction(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("Firebase","Error_Read", databaseError.toException());
                listener.onReaction(null);
            }
        });
    }

    @Override
    public void add(Vehicle vehicle) {

    }

    public void add(String id,Vehicle vehicle) {
        firebaseDatabase.getReference().child("drivers").child(id).child("vehiclesList").child(vehicle.getPlate()).setValue(vehicle);
    }

    @Override
    public String newElement() {
        return null;
    }

    @Override
    public void erase(String id) {

    }

    @Override
    public void update(String id, Vehicle vehicle) {

    }

    @Override
    public void size(SizeListener listener) {

    }
}
