package com.estructurasdatos.guils.model.CRUD_RT;

import android.util.Log;

import androidx.annotation.NonNull;

import com.estructurasdatos.guils.interfaces.CRUDInterfaceRT;
import com.estructurasdatos.guils.model.Chat;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class ChatsFirebase implements CRUDInterfaceRT<Chat> {
    private FirebaseDatabase db;
    private DatabaseReference node;

    public ChatsFirebase() {
        db = FirebaseDatabase.getInstance();
        node = db.getReference().child("chats");
    }

    @Override
    public void element(final String id,final  ElementListener<Chat> listener) {
        final String id_ = id;
        node.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                final Chat chat = dataSnapshot.getValue(Chat.class);
                listener.onReaction(chat);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("Firebase","Error_Read", databaseError.toException());
                listener.onReaction(null);
            }
        });
    }

    @Override
    public void add(Chat chat) {
        node.push().setValue(chat);

    }

    public void add(String id,Chat chat){
        node.child(id).setValue(chat);
    }

    @Override
    public String newElement() {
        return node.push().getKey();
    }

    @Override
    public void erase(String id) {
        node.child(id).setValue(null);
    }

    @Override
    public void update(String id, Chat chat) {
        node.child(id).setValue(chat);
    }

    @Override
    public void size(final SizeListener listener) {
        node.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listener.onReaction(dataSnapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("Firebase","Error_size", databaseError.toException());
                listener.onReaction(-1);
            }
        });
    }
}
