package com.estructurasdatos.guils.model;

import com.estructurasdatos.guils.data_Structure.DynamicArray;

public class Chat{
    private Driver driver;
    private DynamicArray<User> passangerList;
    private DynamicArray<Message> messageList;

    public Chat(Driver driver, DynamicArray<User> passangerList, DynamicArray<Message> messageList) {
        this.driver = driver;
        this.passangerList = passangerList;
        this.messageList = messageList;
    }

    public Chat() {
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public DynamicArray<User> getPassangerList() {
        return passangerList;
    }

    public void setPassangerList(DynamicArray<User> passangerList) {
        this.passangerList = passangerList;
    }

    public DynamicArray<Message> getMessageList() {
        return messageList;
    }

    public void setMessageList(DynamicArray<Message> messageList) {
        this.messageList = messageList;
    }


}
