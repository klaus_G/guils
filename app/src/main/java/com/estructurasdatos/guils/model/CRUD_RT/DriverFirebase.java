package com.estructurasdatos.guils.model.CRUD_RT;

import android.util.Log;

import androidx.annotation.NonNull;

import com.estructurasdatos.guils.interfaces.CRUDInterfaceRT;
import com.estructurasdatos.guils.model.Driver;
import com.estructurasdatos.guils.model.Vehicle;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.security.InvalidKeyException;

public class DriverFirebase implements CRUDInterfaceRT<Driver> {

    private FirebaseDatabase db;
    private VehicleFirebase vehicleFirebase;
    private DatabaseReference node;

    public DriverFirebase() {
        db = FirebaseDatabase.getInstance();
        vehicleFirebase=new VehicleFirebase();
        node = db.getReference().child("drivers");
    }

    @Override
    public void element(final String id, final ElementListener<Driver> listener) {
        final String id_ =id;
        node.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                final Driver driver = dataSnapshot.getValue(Driver.class);
                vehicleFirebase.elementFromDriver(id, new VehicleListener() {
                    @Override
                    public void onReaction(DataSnapshot vehicles) {
                        for (DataSnapshot vehicle:
                             vehicles.getChildren()) {
                            Vehicle result=vehicle.getValue(Vehicle.class);
                             if(result!=null) {
                                 try {
                                     driver.getVehiclesList().put(result.getPlate(),result);
                                 } catch (InvalidKeyException e) {
                                     e.printStackTrace();
                                 }
                             }
                        }

                        listener.onReaction(driver);
                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("Firebase","Error_Read", databaseError.toException());
                listener.onReaction(null);
            }
        });
    }

    @Override
    public void add(Driver driver) {
        throw  new UnsupportedOperationException("Not supported yet");
    }

    public void add(String id,Driver driver) {
        node.child(id).setValue(driver);

        Iterable iterator=driver.getVehiclesList().values();
        for(Object o:iterator){
            Vehicle vehicle=(Vehicle) o;
            vehicleFirebase.add(id,vehicle);
        }
    }

    @Override
    public String newElement() {
        return node.push().getKey();
    }

    @Override
    public void erase(String id) {
        node.child(id).setValue(null);
    }

    @Override
    public void update(String id, Driver driver) {
        node.child(id).setValue(driver);
    }

    @Override
    public void size(final SizeListener listener) {
        node.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listener.onReaction(dataSnapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("Firebase","Error_size", databaseError.toException());
                listener.onReaction(-1);
            }
        });
    }
}
