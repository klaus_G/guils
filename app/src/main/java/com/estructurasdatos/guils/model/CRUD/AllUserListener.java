package com.estructurasdatos.guils.model.CRUD;

import com.estructurasdatos.guils.data_Structure.HashTableMap;
import com.estructurasdatos.guils.model.User;

public interface AllUserListener{
    void onReaction(HashTableMap<String, User> users);
}
