package com.estructurasdatos.guils.model.CRUD;

import android.util.Log;

import androidx.annotation.NonNull;

import com.estructurasdatos.guils.interfaces.CRUDInterface;
import com.estructurasdatos.guils.model.Vehicle;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

public class VehiclesFirestore implements CRUDInterface<Vehicle> {

    private CollectionReference vehicles;

    public VehiclesFirestore() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        vehicles=db.collection("vehicles");
    }

    @Override
    public void element(String id, final ElementListener<Vehicle> listener) {
        vehicles.document(id).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    Vehicle vehicle = task.getResult().toObject(Vehicle.class);
                    listener.onReaction(vehicle);
                }else{
                    Log.e("Firebase","Error al leer",task.getException());
                    listener.onReaction(null);
                }
            }
        });
    }

    @Override
    public void add(Vehicle vehicle) {
        vehicles.document().set(vehicle);
    }

    @Override
    public void newElement() {
        vehicles.document().getId();
    }

    @Override
    public void erase(String id) {
        vehicles.document(id).delete();
    }

    @Override
    public void update(String id, Vehicle vehicle) {
        vehicles.document(id).set(vehicle);
    }

    @Override
    public void size(final SizeListener listener) {
        vehicles.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    listener.onReaction(task.getResult().size());
                }else {
                    Log.e("Firebase","Error en tamano",task.getException());
                    listener.onReaction(-1);
                }
            }
        });
    }
}
