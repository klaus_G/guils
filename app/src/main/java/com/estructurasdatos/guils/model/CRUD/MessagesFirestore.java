package com.estructurasdatos.guils.model.CRUD;

import android.util.Log;

import androidx.annotation.NonNull;

import com.estructurasdatos.guils.interfaces.CRUDInterface;
import com.estructurasdatos.guils.model.Chat;
import com.estructurasdatos.guils.model.Message;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

public class MessagesFirestore implements CRUDInterface<Message> {

    private CollectionReference messages;

    public MessagesFirestore() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        messages=db.collection("messages");
    }

    @Override
    public void element(String id, final ElementListener<Message> listener) {
        messages.document(id).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    Message message = task.getResult().toObject(Message.class);
                    listener.onReaction(message);
                }else{
                    Log.e("Firebase","Error al leer",task.getException());
                    listener.onReaction(null);
                }
            }
        });
    }

    @Override
    public void add(Message message) {
        messages.document().set(message);
    }

    @Override
    public void newElement() {
        messages.document().getId();
    }

    @Override
    public void erase(String id) {
        messages.document(id).delete();
    }

    @Override
    public void update(String id, Message message) {
        messages.document(id).set(message);
    }

    @Override
    public void size(final SizeListener listener) {
        messages.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    listener.onReaction(task.getResult().size());
                }else {
                    Log.e("Firebase","Error en tamano",task.getException());
                    listener.onReaction(-1);
                }
            }
        });
    }
}
