package com.estructurasdatos.guils.model.CRUD;

import android.util.Log;

import androidx.annotation.NonNull;

import com.estructurasdatos.guils.data_Structure.HashTableMap;
import com.estructurasdatos.guils.interfaces.CRUDInterface;
import com.estructurasdatos.guils.model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.security.InvalidKeyException;

public class UsersFirestore implements CRUDInterface<User> {

    //private CollectionReference users;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference node;

    public UsersFirestore() {
        //FirebaseFirestore db = FirebaseFirestore.getInstance();
        firebaseDatabase=FirebaseDatabase.getInstance();
        node=firebaseDatabase.getReference().child("users");
        //users=db.collection("users");
    }

    @Override
    public void element(String id, final ElementListener<User> listener) {

        node.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                User user = dataSnapshot.getValue(User.class);
                listener.onReaction(user);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("Firebase","Error_Read", databaseError.toException());
                listener.onReaction(null);
            }
        });

        /*users.document(id).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    User user = task.getResult().toObject(User.class);
                    listener.onReaction(user);
                }else{
                    Log.e("Firebase","Error al leer",task.getException());
                    listener.onReaction(null);
                }
            }
        });*/
    }
    @Override
    public void add(User user) {
        node.push().setValue(user);
    }

    public void getUsers(final AllUserListener listener){
        node.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                HashTableMap<String ,User> users=new HashTableMap<String ,User> ();
                for (DataSnapshot snapshot:
                     dataSnapshot.getChildren()) {
                    try {
                        users.put(snapshot.getKey(),snapshot.getValue(User.class));
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    }

                }

                listener.onReaction(users);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void add(String id,User user) {
        node.child(id).setValue(user);
    }

    @Override
    public void newElement() {
        node.push().getKey();
    }

    @Override
    public void erase(String id) {
        node.child(id).setValue(null);
    }

    @Override
    public void update(String id, User user) {
        node.child(id).setValue(user);
    }

    @Override
    public void size(final SizeListener listener) {
        node.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listener.onReaction(dataSnapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("Firebase", "Error_size", databaseError.toException());
                listener.onReaction(-1);
            }
        });
    }
}
