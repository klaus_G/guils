package com.estructurasdatos.guils.model.CRUD;

import android.util.Log;

import androidx.annotation.NonNull;

import com.estructurasdatos.guils.interfaces.CRUDInterface;
import com.estructurasdatos.guils.model.Trip;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

public class TripsFirestore implements CRUDInterface<Trip> {

    private CollectionReference trips;

    public TripsFirestore() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        trips=db.collection("trips");
    }

    @Override
    public void element(String id, final ElementListener<Trip> listener) {
        trips.document(id).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    Trip trip = task.getResult().toObject(Trip.class);
                    listener.onReaction(trip);
                }else{
                    Log.e("Firebase","Error al leer",task.getException());
                    listener.onReaction(null);
                }
            }
        });
    }

    @Override
    public void add(Trip trip) {
        trips.document().set(trip);
    }

    @Override
    public void newElement() {
        trips.document().getId();
    }

    @Override
    public void erase(String id) {
        trips.document(id).delete();
    }

    @Override
    public void update(String id, Trip trip) {
        trips.document(id).set(trip);
    }

    @Override
    public void size(final SizeListener listener) {
        trips.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    listener.onReaction(task.getResult().size());
                }else {
                    Log.e("Firebase","Error en tamano",task.getException());
                    listener.onReaction(-1);
                }
            }
        });
    }
}
