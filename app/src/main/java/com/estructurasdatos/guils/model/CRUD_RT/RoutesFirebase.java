package com.estructurasdatos.guils.model.CRUD_RT;

import android.util.Log;

import androidx.annotation.NonNull;

import com.estructurasdatos.guils.interfaces.CRUDInterfaceRT;
import com.estructurasdatos.guils.model.Route;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class RoutesFirebase implements CRUDInterfaceRT<Route> {
    private FirebaseDatabase db;
    private DatabaseReference node;

    public RoutesFirebase(){
        db = FirebaseDatabase.getInstance();
        node = db.getReference().child("routes");
    }

    @Override
    public void element(String id, final ElementListener<Route> listener) {
        final String id_=id;
        node.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final Route route = dataSnapshot.getValue(Route.class);
                listener.onReaction(route);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("Firebase","Error_Read", databaseError.toException());
                listener.onReaction(null);
            }
        });
    }

    @Override
    public void add(Route route) {
        node.push().setValue(route);
    }

    public void add(String id, Route route){
        node.child(id).setValue(route);

    }

    @Override
    public String newElement() {
        return node.push().getKey();
    }

    @Override
    public void erase(String id) {
        node.child(id).setValue(null);
    }

    @Override
    public void update(String id, Route route) {
        node.child(id).setValue(route);
    }

    @Override
    public void size(final SizeListener listener) {
        node.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listener.onReaction(dataSnapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("Firebase","Error_size", databaseError.toException());
                listener.onReaction(-1);
            }
        });
    }
}
