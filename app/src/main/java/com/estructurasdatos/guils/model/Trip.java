package com.estructurasdatos.guils.model;

import java.sql.Time;

public class Trip {
    private String routeId;
    private String days;
    private Time time;
    private int passengers;
    private int priority;

    public Trip(String routeId, String days, Time time, int passengers, int priority) {
        this.routeId = routeId;
        this.days = days;
        this.time = time;
        this.passengers = passengers;
        this.priority = priority;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public int getPassengers() {
        return passengers;
    }

    public void setPassengers(int passengers) {
        this.passengers = passengers;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}
